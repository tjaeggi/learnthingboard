#define BUTTON_PIN_BITMASK 0x8000             
#define TOUCH_PIN0      T0                 // Touch 0 an GPIO4
#define TOUCH_PIN6      T6                 // Touch 6 an GPIO14
#define TIME_TO_SLEEP   5 * 1000000        

RTC_DATA_ATTR int bootCount = 0; 
int loopCount = 0;                        
int schwellenWert = 40;

void printWeckQuelle(){
  esp_sleep_wakeup_cause_t weckQuelle = esp_sleep_get_wakeup_cause();
  switch(weckQuelle)   {
    case ESP_SLEEP_WAKEUP_EXT0 : 
      Serial.println("Weckquelle ist ein externes RTC_IO-Signal"); 
      break;
    case ESP_SLEEP_WAKEUP_EXT1 : 
      Serial.println("Weckquelle ist ein externes RTC_CNTL-Signal"); 
      break;
    case ESP_SLEEP_WAKEUP_TIMER : 
      Serial.println("Weckquelle ist ein Timer");
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD :
    case ESP_SLEEP_WAKEUP_TOUCHPAD :
      Serial.println("Weckquelle ist ein Touch-Sensor"); 
      printWeckQuelleTouchSensor();
      break;
    case ESP_SLEEP_WAKEUP_ULP : 
      Serial.println("Weckquelle ist ein ULP-Programm"); 
      break;
    default : 
      Serial.printf("Weckquelle undefiniert: %d\n", weckQuelle); 
      break;
  }
}

// Ausgeben des Touch-Sensors
void printWeckQuelleTouchSensor(){
  touch_pad_t touchPinWakeUp = esp_sleep_get_touchpad_wakeup_status();
  switch(touchPinWakeUp) {
    case 0  : Serial.println("Auslöser Touch-Sensor 0 an GPIO 4"); break;
    case 6  : Serial.println("Auslöser Touch-Sensor 6 an GPIO 14"); break;
    default : Serial.println("auslösender Touch-Sensor unbekannt"); break;
  }
}

void callback(){                            // Platzhalter für weitere Befehle
}

void setup(){
  Serial.begin(115200);
  delay(1000);                               
  ++bootCount;                              
  printWeckQuelle();                        // Weckquelle ausgeben
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP); 
  touchAttachInterrupt(T0, callback, schwellenWert);  // T0 als Sensor 
  touchAttachInterrupt(T6, callback, schwellenWert);  // T3 als Sensor 
  esp_sleep_enable_touchpad_wakeup();       // Touch-Sensoren als Weckquelle 
}

void loop(){        
  ++loopCount;
  Serial.print  ("bootCount " + String(bootCount) );
  Serial.println(" / loopCount " + String(loopCount) ); 
  Serial.println("Start Deep-Sleep-Modus");       
  delay (500);                               
  esp_deep_sleep_start();                    
  Serial.println("loop Ende");
}
