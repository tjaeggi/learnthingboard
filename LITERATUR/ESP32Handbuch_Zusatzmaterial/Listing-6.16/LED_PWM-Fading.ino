#define LEDC_KANAL_0      0       // Kanal 0; einer der 16-LEDC-Kanäle
#define LEDC_AUFLOESUNG   8       // 8-Bit Tastverhältnisauflöung (0-255)
#define LEDC_FREQ         5000    // 5.000 Hz Frequenz
#define LED_PIN           2       // GPIO2
int maxTV = pow(2,LEDC_AUFLOESUNG)-1;  // maximale Anzahl „Teilschritte“ Tastverhältnis

void setup() {
  // Timer-Setup und Zuordnen des Timers zu einem GPIO-Pin
  ledcSetup(LEDC_KANAL_0, LEDC_FREQ, LEDC_AUFLOESUNG);
  ledcAttachPin(LED_PIN, LEDC_KANAL_0);
}

void loop() {
  for (int i = 0; i <= maxTV; i++) {
    ledcWrite(LEDC_KANAL_0, i);           
    delay (30);
  }
  delay (1000);
  for (int i = maxTV; i > -1; i--) {
    ledcWrite(LEDC_KANAL_0, i);           
    delay (30);
  }
  delay (1000);       
}

