void zeichneTFT() {
  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 30);
  tft.setTextColor(ST77XX_MAGENTA);
  tft.setTextSize(2);
  tft.println("Display ");
  tft.println("color");
  delay (2000);

  tft.fillScreen(ST77XX_BLACK);  
  int row, col, buffidx=0;
  for (row=0; row < farben_height; row++) {               
    for (col=0; col < farben_width; col++) {              
    tft.drawPixel(col, row, pgm_read_word(farben + buffidx));
    buffidx++;
    }               // end pixel
  } 
  delay (3000);  
  
  tft.invertDisplay(true);
  delay(1500);
  tft.invertDisplay(false);
  delay(1500);    
}    
