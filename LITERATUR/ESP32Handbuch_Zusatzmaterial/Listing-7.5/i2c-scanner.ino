#include <Wire.h>

#define SDA2 17
#define SCL2 16

TwoWire i2cScan2 = TwoWire(1);

void scan(int kanal){
  int nDevices = 0;
  byte error;
  for (int i=0; i < 128; i++){
    if (kanal == 1) {
      Wire.beginTransmission(i);
      error = Wire.endTransmission(true);
    } else {   
      i2cScan2.beginTransmission(i);
      error = i2cScan2.endTransmission(true);       
    }
    if (error == 0) {
      Serial.print("I2C-Gerät an Adresse 0x");
      if (i < 16)    Serial.print("0");
      Serial.println(i, HEX); 
      nDevices++;
    }    
  }                        //end for
  
  if (nDevices == 0) {
    Serial.println("Kein I2C-Gerät gefunden\n");
  } else {
    Serial.println("Ende \n");  
  }    
  return;      
}

void setup(){
  Serial.begin(115200);
  Wire.begin();                     // SDA  Pin 21, SCL  Pin 22   
  i2cScan2.begin(SDA2,SCL2,400000); // SDA2 Pin 17, SCL2 Pin 16 
}

void loop(){
  Serial.println("Start I2C-Scan Kanal 1");
  scan (1);
  delay(100);
  Serial.println("Start I2C-Scan Kanal 2");    
  scan (2);
  Serial.println();
  delay(5000);
}
