#include <WiFi.h>                           // für WiFi
#include <HTTPClient.h>
#include <WiFiUdp.h>
#include <NTPClient.h>                      // für NTP
#include <ArduinoJson.h>                    // JSON aufbereiten
#include <ESP32Ping.h>                      // Ping

#define LED_PIN              2              // GPIO2, PIN G2
#define PUMPE_PIN            5              // GPIO5, PIN G5
#define BODEN_SENSOR_A_PIN   33             // GPIO33, PIN G33
#define NTP_OFFSET 1 * 60 * 60              // in Sekunden (1* = Winterzeit, 2* = Sommerzeit)
#define NTP_INTERVAL 60 * 1000              // in Millisesekunden
#define NTP_ADDRESS "3.de.pool.ntp.org"     // "ptbtime1.ptb.de" //   "europe.pool.ntp.org"

const char* ssid     = "IhreSSID";
const char* password = "IhrRouterPW";
IPAddress lclIP (192,168,2,203);            // Test
IPAddress gateway (192,168,2,1);
IPAddress subnet (255,255,255,0);
IPAddress primaryDNS (8, 8, 8, 8);          // optional
IPAddress secondaryDNS (8, 8, 4, 4);        // optional

//WiFiServer serverWiFi (80);                 // Port für Webserver
WiFiServer serverWiFi (8888);                 // Port für Webserver
WiFiClient wifiClient;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

// Variablen für Ping
IPAddress ipPing (192, 168, 2, 184);        // Huawei Handy 

// Variablen für Timer Bewässerung
hw_timer_t *timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// Variablen für kapazitiven Bodenfeuchtemesser
int schwellenWert = 40;                     // Bodenfeuchtigkeit Schwellenwert (anpassen)
int feuchtigkeit = 0;                       // Bodenfeuchtigkeit in %
volatile bool pumpenStatus = false;         // false = aus, true = an

// Variablen für Wetterdaten
String stadtLand = "Berlin,de";             // Ort Stadt und Länderkennung
const String url = "http://api.openweathermap.org/data/2.5/weather?q=";
const String key = "f90d72fb3fa99ef3cfdf6339e9b0a816";
unsigned long millisAlt = 0;                // Abruf Wetterdaten begrenzen
unsigned long sunset, sunrise;
bool bewOK = false;                         // false = zeitlich keine Bewässerung


// Variablen für Beleuchtungssteuerung Anwesenheit
bool ledCtrl = false;                       // automatisch = 0, manuell = 1
volatile bool cntrlPing = false;            // 1 = warten nächster Intervall 

// Variablen für Timer Ping
hw_timer_t *timerPing = NULL;
portMUX_TYPE timerMuxPing = portMUX_INITIALIZER_UNLOCKED;

// Timer Pumpe
void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  pumpenStatus = false;
  portEXIT_CRITICAL_ISR(&timerMux);  
  digitalWrite(PUMPE_PIN, LOW);  
}

// lesen der Messwerte 
bool lesenMesswert() {
  int analogValue = analogRead(BODEN_SENSOR_A_PIN);
  feuchtigkeit = map(analogValue, 0, 4095, 100, 0); 
  if (feuchtigkeit >= schwellenWert) {
    return false;
  } else {
    return true;
  }
}

// prüfeb, ob Bewässern zeitlich möglich: wenn ja durchführen
void checkBewaesserung() {
  if (lesenMesswert()) {                     // zu trocken
    if (!pumpenStatus) {                     // Wasser-Pumpe = aus
      if (millis() > millisAlt + (1000*60*5 )) {     // Wetterdaten alle 5 Minute
        bewOK = holenWetterDaten();          // holen der Wetterdaten über das Netz
        millisAlt = millis();    
      }   
      if (bewOK) {     
        digitalWrite(PUMPE_PIN, HIGH);
        portENTER_CRITICAL(&timerMux);
        pumpenStatus = true;
        portEXIT_CRITICAL(&timerMux); 
        timer = timerBegin(1, 80, true);
        timerAttachInterrupt(timer, &onTimer, true);
        timerAlarmWrite(timer, 5000000, false);  // für 5 Sekunden
        timerAlarmEnable(timer);
      }                                      // endif bewOK
    }                                        // endif pumpenStatus                                    
  }                                          // endif lesenMesswert
  return;
}  

// holen der aktuellen Zeit
unsigned long holenNTP() {
  timeClient.update();                      // aktualisieren des Zeitstempels
  return timeClient.getEpochTime(); 
}

// holen der Wetterdaten über das Netz
bool holenWetterDaten () {
  bool ret = false;
  if ((WiFi.status() == WL_CONNECTED)) {     // besteht Verbindung noch?
    HTTPClient http;
    http.begin(url + stadtLand + "&units=metric&APPID=" + key);     
    int httpCode = http.GET();               // Request absetzen
    if (httpCode > 0) {                      // returning code ok? (id.R. 200)
      String payload = http.getString();    
      StaticJsonDocument<1000> jsonDocument;     // Speicher für JSON bereit stellen
      auto err = deserializeJson(jsonDocument, payload); // JSON aufbereiten
      if (err) {         
        Serial.print  ("Fehler JSON-deserialize  ");
        Serial.println(err.c_str());
        ret = false;
      } else {     
        sunrise = jsonDocument ["sys"] ["sunrise"];  
        sunset = jsonDocument ["sys"] ["sunset"];        
        unsigned long epochZeit = holenNTP(); 
        if (epochZeit < sunrise || epochZeit > sunset) {   
          ret = true;    
        } else {
          ret = false;
        }
      }
    } else {
      Serial.println("Fehler beim HTTP request");
      ret = false;
    }  
    http.end();                              // Ressourcen frei geben
  }  
  return ret;
}

// Timer Ping 
void IRAM_ATTR onTimerPing () {
  portENTER_CRITICAL_ISR(&timerMuxPing);
  cntrlPing = false;
  portEXIT_CRITICAL_ISR(&timerMuxPing);   
}

// Ping absetzen und auswerten
void checkPing() {
  static bool ret; 
  if (!cntrlPing) {                          // kein aktueller Ping
    ret = Ping.ping(ipPing);
    portENTER_CRITICAL(&timerMuxPing);
    cntrlPing = true;
    portEXIT_CRITICAL(&timerMuxPing); 
    timerPing = timerBegin(0, 80, true);
    timerAttachInterrupt(timerPing, &onTimerPing, true);
    timerAlarmWrite(timerPing, 5000000, false);  // für 5 Sekunden
    timerAlarmEnable(timerPing);  
  }          

  if (ret) {                                 // Ping erfolgreich 
    if (!ledCtrl) {                          // Automatik an ?
      digitalWrite(LED_PIN, HIGH);
    }    
  } else {
    if (!ledCtrl) {                          // Automatik an ?
      digitalWrite(LED_PIN, LOW);
    }    
  }
  return;
}

// sendet eine Antwort an den WiFi-Client
void wifiSend (WiFiClient client) {  
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html"); 
  client.println("Connection: close");  
  client.println();  
  
  // der Inhalt der HTTP-Antwort folgt auf den HTTP-Header
  // einfache Ausgabe:
  client.print ("<!DOCTYPE HTML>");
  client.print("Bodenfechtigkeit: " + String(feuchtigkeit) + " %<br>");
  client.print("LED-Control   <a href=\"/A\">automatisch</a> / <a href=\"/M\">manuell</a><br>");
  client.print("-> bei manuell  <a href=\"/H\">an</a> oder <a href=\"/L\">aus</a> !");
  client.println("</body>\n");
  client.println("</html>");

  // die HTTP-Antwort endet mit einer weiteren Leer-Zeile
  client.println();    
}

// bearbeitet eine Anfrage von dem WiFi-Client
void wifiReceive (WiFiClient client) {
  Serial.println("Neue Anfrage.");          // Meldung im Seriellen Monitor
  String currentLine = "";                  // Variable f. eingehende Daten
  while (client.connected()) {              // loop solange client verbunden
    if (client.available()) {               // gibt es Bytes zu lesen   
      char c = client.read();               // lesen 1 Byte in Varaible c
      //Serial.print(c);                      // Ausgabe des Bytes 
      if (c == '\n') {                      // ist Zeichen newline-Zeichen ?
        // das Ende der HTTP-Anfrage ist eine Leer-Zeile 
        // und zwei newline Zeichen hintereinander
        if (currentLine.length() == 0) {    // Ende HTTP-Anforderung Client        
          wifiSend(client);             
          break;                            // beenden while client.connected 
        } else {                            // liegt ein newline vor
         currentLine = "";                  // Variable currentLine löschen
        }
      } else if (c != '\r') {               // alles nur kein Wagenrücklauf
        currentLine += c;                   // Zeichen currentLine hinzufügen
      }
      // Auswertung der Client-Anfrage  
      if (currentLine.endsWith("GET /H") &&
         ledCtrl == true) {
        digitalWrite(LED_PIN, HIGH);        // GET /H schaltet die LED an
      }
      if (currentLine.endsWith("GET /L") &&
         ledCtrl == true) {
        digitalWrite(LED_PIN, LOW);         // GET /L schaltet die LED aus
      }
      if (currentLine.endsWith("GET /A")) {
        ledCtrl = false;                    // GET /A LED-Automatik ein
      }
      if (currentLine.endsWith("GET /M")) {
        ledCtrl = true;                     // GET /M LED-Automatik aus
      }        
    }                                       // end if client.available
  }                                         // end while client.available

  client.stop();     
  Serial.println("Client Disconnected.");  
  Serial.println();      
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT); 
  pinMode(PUMPE_PIN, OUTPUT);  
  if (!WiFi.config(lclIP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA failed to configure ");    
  }  
  delay(10);
  Serial.print("Verbindungsaufbau zu ");    // Verbindungsaufbau zum WiFi-Netzwerk
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }  
  Serial.println("");                       // Verbindung aufgebaut
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.print  (WiFi.localIP());
  Serial.println();  
  Serial.println("Start WiFi Server!");  
  serverWiFi.begin();
  
  bewOK = holenWetterDaten(); 

}

void loop() {
  checkPing();                              // ist Teilnehmer erreichbar
  checkBewaesserung();
  WiFiClient client = serverWiFi.available();  // horcht auf Client-Anfragen
  if (client) {                             // fragt ein Client an?
    wifiReceive (client);                   // Anfrage aufbereiten
  }       
  delay(1);
}