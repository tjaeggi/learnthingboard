#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#define BLINK_LED1    2
#define BLINK_LED2    4
 
TaskHandle_t task1;
TaskHandle_t task2;

// task1Code: LED blinkt alle 1,5 Sekunden
void task1Code( void * pvParameters ){
  printf ("Task 1 wird ausgeführt in Kern %d\n", xPortGetCoreID());
  for(;;){
    gpio_set_level(BLINK_LED1, 0);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    gpio_set_level(BLINK_LED1, 1);
    vTaskDelay(1000 / portTICK_PERIOD_MS); 
  } 

}

//task2Code: LED blinkt alle 0,7 Sekunden
void task2Code( void * pvParameters ){
  printf ("Task 2 wird ausgeführt in Kern %d\n", xPortGetCoreID());
  for(;;){
    gpio_set_level(BLINK_LED2, 0);
    vTaskDelay(500 / portTICK_PERIOD_MS);
    gpio_set_level(BLINK_LED2, 1);
    vTaskDelay(500 / portTICK_PERIOD_MS); 
  } 
}

void app_main() {
  gpio_pad_select_gpio(BLINK_LED1);
  gpio_pad_select_gpio(BLINK_LED2);
  gpio_set_direction(BLINK_LED1, GPIO_MODE_OUTPUT);
  gpio_set_direction(BLINK_LED2, GPIO_MODE_OUTPUT);    
  int core = xPortGetCoreID();
  printf ("main() wird ausgeführt in Kern %d\n", core);

  // erstellt einen Task, der in der Funktion task1Code()
  // mit der Priorität 1 und auf Kern 0 ausgeführt wird
  xTaskCreatePinnedToCore(
        task1Code,     // Task-Funktion
        "Task1",       // Task-Name
        10000,         // Stack-Größe des Tasks 
        NULL,          // Task-Parameter
        1,             // Task-Priorität
        &task1,        // Task-Handler für den eingerichteten Task
        0);            // Task geht an core 0                  
  vTaskDelay(500 / portTICK_PERIOD_MS);

  // erstellt einen Task, der in der Funktion task2Code()
  // mit der Priorität 1 und auf Kern 2 ausgeführt wird
  xTaskCreatePinnedToCore(
        task2Code,     // Task-Funktion
        "Task2",       // Task-Name
        10000,         // Stack-Größe des Tasks 
        NULL,          // Task-Parameter
        1,             // Task-Priorität
        &task2,        // Task-Handler für den eingerichteten Task
        1);            // Task geht an core 1 

  vTaskDelay(5000 / portTICK_PERIOD_MS);	
  printf ("Anzahl Tasks     %d\n", uxTaskGetNumberOfTasks());
  vTaskDelete (task1);
  vTaskDelay(1000 / portTICK_PERIOD_MS); 	
  printf ("Anzahl Tasks neu %d\n", uxTaskGetNumberOfTasks());

  while(1) { 
    // vermeidet Fehlermeldungen im Monitor
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}
