#define PIR_SENSOR_PIN     13         // Pin für den PIR-Sensor
int pirStatusAlt = LOW;               // PIR-Status sichern

// PIR-Ausgang lesen, auswerten und Aktion durchführen
void lesenPIR(){ 
  if (digitalRead(PIR_SENSOR_PIN)) {  // PIR lesen und Status prüfen   
    if (pirStatusAlt == LOW) {
      Serial.println("Bewegung erkannt!");
      pirStatusAlt = HIGH;            // Status sichern
      delay (100);
     }
  } else {
    if (pirStatusAlt == HIGH){
      Serial.println("Bewegung vorbei!");
      pirStatusAlt = LOW;
      delay (100);
    }
  }
}
    
void setup() {
  pinMode(PIR_SENSOR_PIN, INPUT);     // öffnet Pin für den Sensor als Input
  Serial.begin(115200);
}
     
void loop(){
  lesenPIR();
  delay (10);
}
