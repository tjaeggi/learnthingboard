#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_sleep.h"
#include "rom/uart.h"
#include "driver/rtc_io.h"

void app_main() {
    gpio_config_t config = {
            .pin_bit_mask = BIT64(0),
            .mode = GPIO_MODE_INPUT
    };
    gpio_config(&config);
    gpio_wakeup_enable(0, GPIO_INTR_LOW_LEVEL);
    while (true) {
        esp_sleep_enable_gpio_wakeup();
        printf("Taster loslassen für erneuten Schlafmodus...\n");
        do {
            vTaskDelay(pdMS_TO_TICKS(10));
        } while (rtc_gpio_get_level(0) == 0);
        printf("Beginn Light Sleep\n");
        uart_tx_wait_idle(CONFIG_CONSOLE_UART_NUM);
        int64_t t_before_us = esp_timer_get_time();
        esp_light_sleep_start();
        int64_t t_after_us = esp_timer_get_time();
        const char* wakeup_reason;
        switch (esp_sleep_get_wakeup_cause()) {
            case ESP_SLEEP_WAKEUP_GPIO:
                wakeup_reason = "pin";
                break;
            default:
                wakeup_reason = "andere";
                break;
        }
        printf("Wakeup Light Sleep, Weckgrund: %s, t=%lld ms, slept for %lld ms\n",
                wakeup_reason, t_after_us / 1000, (t_after_us - t_before_us) / 1000);
    }
}
