#define WD_PIN       4
hw_timer_t *timer = NULL;

void IRAM_ATTR restartESP32() {
  ESP.restart();
}

void setup() {
  Serial.begin(115200);
  pinMode(WD_PIN, INPUT_PULLUP);                    
  timer = timerBegin(0, 80, true);                  
  timerAttachInterrupt(timer, &restartESP32, true);  
  timerAlarmWrite(timer, 3000000, false);           
  timerAlarmEnable(timer);                          
}

void loop() { 
  timerWrite(timer, 0); 
  long millisLoop = millis();  
  while (!digitalRead(WD_PIN)) {
    long millisWhile = millis() - millisLoop;
    Serial.println("Aufenthalt " + String (millisWhile) + " mSek");
    delay(300);
  }
  delay(1000);                  // weitere Programmausführung simulieren
  millisLoop = millis() - millisLoop; 
  Serial.print  ("Zeit seit Reset Watchdog = ");
  Serial.println(String(millisLoop) + " mSek");   // sollte unter 3000 sein
}
