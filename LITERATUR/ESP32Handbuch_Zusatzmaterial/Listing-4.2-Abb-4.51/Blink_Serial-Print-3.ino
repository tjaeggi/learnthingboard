#include "pause.h"
void setup() {
  Serial.begin (115200);      
  pinMode(2, OUTPUT);
}

void loop() {
  digitalWrite(2, HIGH);  
  Serial.println ("LED an");
  pausieren ();     
  digitalWrite(2, LOW);   
  Serial.println ("LED aus");
  pausieren (); 
}
