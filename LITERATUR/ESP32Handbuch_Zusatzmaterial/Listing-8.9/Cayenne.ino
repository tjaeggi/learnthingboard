#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP32.h>       // Header Cayenne
#include <Wire.h>
#include <Adafruit_Sensor.h>        
#include <Adafruit_BMP280.h>        

#define LED_PIN    2                // GPIO2, Pin G2

const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
char username[] = "8ef6b600-2f90-11e9-809d-0f8fe4cXXXX";
char password[] = "5f72ee148533c416a3aa194c311131507eeXXXX";
char clientID[] = "03121e60-52ec-11ea-b301-fd142d6XXXX";

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
int ledStatus = 0;

CAYENNE_OUT_DEFAULT() { 
  Cayenne.celsiusWrite(0, bmp.readTemperature());
  Cayenne.virtualWrite(1, bmp.readPressure() / 100);
  Cayenne.digitalSensorWrite(2,ledStatus);
}

CAYENNE_IN_DEFAULT() {
  if (request.channel == 3) {
    ledStatus = getValue.asInt();
    digitalWrite(LED_PIN,ledStatus);
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);  
  digitalWrite(LED_PIN, LOW);  
  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  } 
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);
}

void loop() {
  Cayenne.loop();
}
