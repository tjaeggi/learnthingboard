// Quelle: Arduino   DATEI • BEISPIEL • ESP32 • ESP32NOW • BASIC • SLAVE
#include <esp_now.h>
#include <WiFi.h>

#define CHANNEL 1

uint8_t masterMac [6];     // masterMac [] = {0xFC, 0x71, 0xBF, 0x9D, 0xDD, 0xEC};
esp_now_peer_info_t master; 
const esp_now_peer_info_t *masterNode = &master;

//Datenstruktur für den Datenaustausch
struct betriebsDaten {
    uint32_t cnt = 0;  
    uint8_t  sekunden = 0;
    uint8_t  minuten = 0;
    uint8_t  stunden = 0;        
};
uint32_t cnt = 0;
uint8_t data = 0;

// Init ESP Now with fallback
void InitESPNow() {
//  WiFi.disconnect();
  if (esp_now_init() == ESP_OK) {
    Serial.println("ESPNow Init Success");
  }
  else {
    Serial.println("ESPNow Init Failed");
    // Retry InitESPNow, add a counte and then restart?
    // InitESPNow();
    // or Simply Restart
    ESP.restart();
  }
}

// Check if the slave is already paired with the master.
// If not, pair the slave with master
bool manageMaster() {   
  if (master.channel == CHANNEL) {
    Serial.print("Master status:           ");
    // check if the peer exists
    bool exists = esp_now_is_peer_exist(master.peer_addr);
    if ( exists) {
      // Master already paired.
      Serial.println("Already Paired");
      return true;
    } else {
      // Master not paired, attempt pair
      esp_err_t addStatus = esp_now_add_peer(&master);
      if (addStatus == ESP_OK) {
        // Pair success
        Serial.println("Pair success");
        return true;
      } else if (addStatus == ESP_ERR_ESPNOW_NOT_INIT) {
        // How did we get so far!!
        Serial.println("ESPNOW Not Init");
        return false;
      } else if (addStatus == ESP_ERR_ESPNOW_ARG) {
        Serial.println("Invalid Argument");
        return false;
      } else if (addStatus == ESP_ERR_ESPNOW_FULL) {
        Serial.println("Peer list full");
        return false;
      } else if (addStatus == ESP_ERR_ESPNOW_NO_MEM) {
        Serial.println("Out of memory");
        return false;
      } else if (addStatus == ESP_ERR_ESPNOW_EXIST) {
        Serial.println("Peer Exists");
        return true;
      } else {
        Serial.println("Not sure what happened");
        return false;
      }
    }
  } else {
    // No master found to process
    Serial.println("No master found to process");
    return false;
  }
}

// send data
void sendData() {
  betriebsDaten bd;  
  bd.cnt = cnt;
  bd.sekunden = (millis()/1000) % 60;
  bd.minuten  = (millis()/(1000*60)) % 60;
  bd.stunden  = (millis()/(1000*60*60)) % 24;  
  // kopieren der daten in einen Speicherblock und senden sie zurück an den Absender
  uint8_t s_data[sizeof(bd)]; 
  memcpy(s_data, &bd, sizeof(bd));
  esp_err_t ret = esp_now_send(master.peer_addr, s_data, sizeof(s_data));
  
  if (ret != ESP_OK ){
    Serial.print ("Error sending message!! ");
    if (ret == ESP_ERR_ESPNOW_NOT_INIT) Serial.println("ESPNOW is not initialized");
    if (ret == ESP_ERR_ESPNOW_ARG) Serial.println("invalid argument");
    if (ret == ESP_ERR_ESPNOW_INTERNAL) Serial.println("internal error");
    if (ret == ESP_ERR_ESPNOW_NO_MEM) Serial.println("out of memory");
    if (ret == ESP_ERR_ESPNOW_NOT_FOUND) Serial.println("peer is not found");
    if (ret == ESP_ERR_ESPNOW_IF) Serial.println("current WiFi interface doesn’t match that of peer");
  } else {
    Serial.print  ("send back to master:     " + String (bd.cnt) );
    Serial.print  (" " + String (bd.stunden) );
    Serial.print  ("." + String (bd.minuten) );
    Serial.println("." + String (bd.sekunden) );
  }     
}

// callback when data is sent from Master to Slave
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  char macStr[18];
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print("Last Packet Sent to:     "); 
  Serial.println(macStr);
  Serial.print("Last Packet Send Status: "); 
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  Serial.println();
}

// callback when data is recv from Master
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *r_data, int data_len) {
  char macStr[18];
  uint8_t inData;    
  betriebsDaten bd;    
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], 
           mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print("Last Packet Recv from:   "); 
  Serial.println(macStr);
  memcpy(&bd, r_data, sizeof(bd));
  Serial.print  ("received from master:    " + String (bd.cnt) );
  Serial.print  (" " + String (bd.stunden) );
  Serial.print  ("." + String (bd.minuten) );
  Serial.println("." + String (bd.sekunden) );

  // add master as peer if it has not been added already
  memcpy (&master.peer_addr, mac_addr, 6);  
  bool isPaired = manageMaster();
  if (isPaired) {
    // pair success or already paired, send data back to master  
    sendData();
  } else {
    // slave pair failed
    Serial.println("Master pair failed!");
  }

}


void setup() {
  Serial.begin(115200);
  Serial.println("ESPNow/Basic/Slave Example");
  //Set device in AP mode to begin with
  WiFi.mode(WIFI_AP);
  // configure device AP mode
  const char *SSID = "Slave_1";
  bool result = WiFi.softAP(SSID, "Slave_1_Password", CHANNEL, 0);
  if (!result) {
    Serial.println("AP Config failed.");
  } else {
    Serial.println("AP Config Success, AP: " + String(SSID));
  }
  
  // This is the mac address of the Slave in AP Mode
  Serial.print("AP MAC : ");
  Serial.println(WiFi.softAPmacAddress());
  Serial.print("STA MAC: ");
  Serial.println(WiFi.macAddress());
   // Init ESPNow with a fallback logic
  InitESPNow();

  master.channel = CHANNEL;
  master.ifidx = ESP_IF_WIFI_AP;

  // register callback send and receive
  esp_now_register_send_cb(OnDataSent);  
  esp_now_register_recv_cb(OnDataRecv);
  Serial.println();  
}

void loop() {
  cnt++;  
  delay(1500);
}