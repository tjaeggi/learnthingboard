// vgl. Arduino-IDE : DATEI • BEISPIELE • ESP32 MAIL CLIENT - SEND_EMAIL
#include <Arduino.h>
#include "ESP32_MailClient.h"

#define WIFI_SSID         " ssidRouter"
#define WIFI_PASSWORD     " passwortRouter"

SMTPData smtpData;                    // Instanz "senden" für Gsender 

// Callback Sendestatus
void sendCallback(SendStatus msg) {
  Serial.println(msg.info());         // Sendestatus ausgeben
  if (msg.success()) {                // weitere Aktionen bei Erfolg
    Serial.println("----------------");
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.print("Connecting to AP");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(200);
  }

  Serial.println();
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  Serial.println("E-Mail senden..");

  // Setzen von E-Mail-Host, -Port, -Account und -Passwort
  smtpData.setLogin("smtp.gmail.com", 587, "br@gmail.com", "passw");
  smtpData.setSTARTTLS(true);

  // Setzen von Sender-Name und -E-Mail-Adresse
  smtpData.setSender("ESP32", "br@gmail.com");

  // Setzen der E-Mail-Priorität (niedrig 1 bis hoch 5)
  smtpData.setPriority("High");
  
  // Setzen der Überschrift
  smtpData.setSubject("ESP32 SMTP-Mail Test");

  // Setzen des E-Mail-Inhalts
  smtpData.setMessage("<div style=\"color:#ff0000;font-size:20px;\">Dies ist ein E-Mail-Test</div>", true);

  // Setzen des Empfängers
  smtpData.addRecipient("br@gmail.com");

  // Callback registrieren
  smtpData.setSendCallback(sendCallback);

  // Senden der E-Mail
  if (!MailClient.sendMail(smtpData))
    Serial.println("Fehler beim Senden, " + MailClient.smtpErrorReason());

  // E-Mail-Datenbereich löschen und Speicher frei geben
  smtpData.empty();
}

void loop() {}
