#include "DHTesp.h"                         // DHT22-Bibliothek für ESP32
#define DHT22_PIN 13                        // Pin für den DHT22-Sensor 
DHTesp dht;                                 // Objekt anlegen

// DHT22-Messwerte lesen und Aktion ausführen (hier Serial Monitor)
bool lesenDHT22() {
  ComfortState kS;                          // Stufe Raumklima 
  float temperatur = dht.getTemperature();
  float luftFeuchte = dht.getHumidity();  
  //TempAndHumidity dhtData = dht.getTempAndHumidity();
  if (dht.getStatus() != 0) {
    Serial.println("Fehler beim Lesen des DHT-Sensors!");
    return false;
  }
  
  float hIndex = dht.computeHeatIndex(temperatur, luftFeuchte);
  float tPunkt = dht.computeDewPoint(temperatur, luftFeuchte);
  float cr = dht.getComfortRatio(kS, temperatur, luftFeuchte);

  String raumKlima;
  switch(kS) {
    case Comfort_OK:           raumKlima = "perfekt";
                               break;
    case Comfort_TooHot:       raumKlima = "zu warm";
                               break;
    case Comfort_TooCold:      raumKlima = "zu kalt";
                               break;
    case Comfort_TooDry:       raumKlima = "zu trocken";
                               break;
    case Comfort_TooHumid:     raumKlima = "zu feucht";
                               break;
    case Comfort_HotAndHumid:  raumKlima = "warm und feucht";
                               break;
    case Comfort_HotAndDry:    raumKlima = "warm und trocken";
                               break;
    case Comfort_ColdAndHumid: raumKlima = "kalt und feucht";
                               break;
    case Comfort_ColdAndDry:   raumKlima = "kalt und trocken";
                               break;
    default:                   raumKlima = "unbekannt:";
                               break;
  };

  Serial.println("Temperatur:       " + String (temperatur) + " °C" );
  Serial.println("  gefühlt:        " + String (hIndex) + " °C" );
  Serial.println("Luftfeuchtigkeit: " + String (luftFeuchte)  + " %");
  Serial.println("Taupunkt:         " + String (tPunkt)  + " °C");
  Serial.println("Raumklima:        " + raumKlima + "\n");      
  return true;
}

void setup() {
  Serial.begin(115200);
  Serial.println("Start DHT22 sensor!");
  dht.setup(DHT22_PIN, DHTesp::AUTO_DETECT);   // DHTesp::DHT22
}

void loop() {
  bool ret= lesenDHT22();
  delay(2000);
} 
