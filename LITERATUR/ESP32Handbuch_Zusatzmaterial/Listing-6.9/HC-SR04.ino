#define US_SENSOR_TRIG_PIN   33       // Trigger-Pin für den Ultraschallsensor 
#define US_SENSOR_ECHO_PIN   34       // Echo-Pin für den Ultraschallsensor 
float entfernung;                     // berechnete Entfernung
float dauer;                          // Zeitspanne des Echos

void lesenHCSR04() {
  digitalWrite(US_SENSOR_TRIG_PIN, LOW);   // definierten Zustand schaffen
  delayMicroseconds(5);                    // warten auf def. Pin-Zustand
  digitalWrite(US_SENSOR_TRIG_PIN, HIGH);  // Trigger 10 Mikrosekunden ein 
  delayMicroseconds(10);
  digitalWrite(US_SENSOR_TRIG_PIN, LOW); 
  dauer = pulseIn(US_SENSOR_ECHO_PIN, HIGH);    // Pulslänge messen, Mikrosek.
  entfernung = dauer * 0.034 / 2;               // Entfernung berechnen
  Serial.println("Entfernung (cm): " + String (entfernung));
}

void setup() {
  pinMode(US_SENSOR_TRIG_PIN, OUTPUT);
  pinMode(US_SENSOR_ECHO_PIN, INPUT);
  Serial.begin(115200); 
}

void loop() {  
  lesenHCSR04();
  delay(2000);
}
