#define BODEN_SENSOR_A_PIN   33           // Pin für den Bodensensor analog
#define BODEN_SENSOR_D_PIN   27           // Pin für den Bodensensor digital
#define BODEN_SENSOR_VCC_PIN 34           // Pin für VCC Bodensensor

int schwellenWert = 50;                   // anzupassender Schwellenwert 

void lesenYL69() {
  digitalWrite(BODEN_SENSOR_VCC_PIN, HIGH); 
  delay (1000);
  int analogValue = analogRead(BODEN_SENSOR_A_PIN);
  int digitalValue = digitalRead(BODEN_SENSOR_D_PIN);
  //map Analogwert: 100% - sehr nass / 0% - sehr trocken
  int feuchtigkeit = map(analogValue, 0, 4095, 100, 0);
  Serial.print("Sensor (digital/analog): ");
  Serial.print(String(digitalValue) + "/");
  Serial.print(analogValue);
  Serial.println("  Feuchtigkeit in Prozent; " + String(feuchtigkeit) + "%");

  if (feuchtigkeit > schwellenWert) {
    Serial.print(" -> Keine Bewässerung nötig");
  } else {
    Serial.print(" -> Die Pflanzen brauchen Wasser");
  }
  if (digitalValue == LOW) {
    Serial.println(" - alles in Ordnung ");   
  } else {     
    Serial.println(" – Alarm! Bewässerung dringend");
  }
  digitalWrite(BODEN_SENSOR_VCC_PIN, LOW); 
}

void setup() {
  pinMode(BODEN_SENSOR_VCC_PIN, OUTPUT);
  pinMode(BODEN_SENSOR_D_PIN, INPUT);
  digitalWrite(BODEN_SENSOR_D_PIN, LOW); 
  Serial.begin(115200); 
}

void loop() {  
  lesenYL69();
  delay(5000);
}
