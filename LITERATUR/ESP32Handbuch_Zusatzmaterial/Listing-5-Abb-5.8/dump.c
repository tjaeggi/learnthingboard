/* Dump Example
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"
void dividieren () {
	for (int i = 10; i > -1; i--) {
		int erg = 10 / i;
		printf("Ergebnis von 10 / %i = %i\n", i, erg);
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }	
	return;
}
void app_main() {
	vTaskDelay(10000 / portTICK_PERIOD_MS);
	dividieren();
}
