#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"
#include "pause.h"

void pausieren () {
  vTaskDelay(1000 /  portTICK_PERIOD_MS);
  return;
}
