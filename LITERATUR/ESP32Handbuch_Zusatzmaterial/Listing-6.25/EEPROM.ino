#include <EEPROM.h>
#define EEPROM_SIZE 1
int i = 0;
int iAlt = 0;

void setup() {
  Serial.begin(115200); 
  // EEPROM mit festgelegter Größe initialisieren
  EEPROM.begin(EEPROM_SIZE);
  // gesicherten Status aus dem Flash-Memory auslesen
  iAlt = EEPROM.read(0);
  Serial.print ("Wert bei setup: "); 
  Serial.println (iAlt); 
  Serial.println ();  
}

void loop() {
  if (Serial.available() > 0) {
    i = Serial.parseInt(); 
    if ((i != iAlt) && (i != 0)){
      Serial.print ("neuer Wert: "); 
      Serial.print (i); 
      Serial.println("  -> speichern !"); 
      Serial.println ();
      iAlt = i;
      EEPROM.write(0, iAlt);
      EEPROM.commit();
    }
  }
}
