#define ADC_PIN   14             // ADC2-7 an GPIO14
int schwellenWert = 3273;       // Schwellenwert 80 % von 4095
void lesenADC () {    
  int anlgRead = analogRead(ADC_PIN);
  Serial.println("gelesener ADC-Wert: " + String (anlgRead) );
  if (anlgRead > schwellenWert) {
    Serial.println("Schwellenwert überschritten !");     
  }
}
void setup() {   
  Serial.begin(115200);    
} 
void loop() {   
  lesenADC();
  delay(1000);    
}
