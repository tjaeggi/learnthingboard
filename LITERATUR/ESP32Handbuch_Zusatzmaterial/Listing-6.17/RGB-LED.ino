#define LEDC_PWM_KANAL_R   0         // Kanal 0 für Rot 
#define LEDC_PWM_KANAL_G   1         // Kanal 1 für Grün 
#define LEDC_PWM_KANAL_B   2         // Kanal 2 für Blau 
#define LEDC_AUFLOESUNG    8         // 8-Bit-Auflösung 
#define LEDC_FREQ          5000      // 5.000 Hz als LEDC-Basis-Frequenz
#define LED_PIN_R          0
#define LED_PIN_G          4
#define LED_PIN_B          5   

uint32_t rgbWerte[7][3] = {
    {255,   0,   0},                 // rot
    {  0, 255,   0},                 // grün
    {  0,   0, 255},                 // blau
    {255, 255, 255},                 // weiß
    {170,   0, 130},                 // violett
    {170, 150,   0},                 // senfgelb
    {  0, 170, 170}                  // türkis
};    

void setup() {
  // Setup der PWM-Timer und Anbinden der Timer an einen LED-Pin
  ledcSetup(LEDC_PWM_KANAL_R, LEDC_FREQ, LEDC_AUFLOESUNG);
  ledcAttachPin(LED_PIN_R, LEDC_PWM_KANAL_R);
  ledcSetup(LEDC_PWM_KANAL_G, LEDC_FREQ, LEDC_AUFLOESUNG);
  ledcAttachPin(LED_PIN_G, LEDC_PWM_KANAL_G);
  ledcSetup(LEDC_PWM_KANAL_B, LEDC_FREQ, LEDC_AUFLOESUNG);
  ledcAttachPin(LED_PIN_B, LEDC_PWM_KANAL_B);    
}

void loop() { 
  for (int i = 0; i < 7; i++) {
    ledcWrite(LEDC_PWM_KANAL_R, rgbWerte[i][0]);
    ledcWrite(LEDC_PWM_KANAL_G, rgbWerte[i][1]);
    ledcWrite(LEDC_PWM_KANAL_B, rgbWerte[i][2]);  
    delay(2000);
  }
}
