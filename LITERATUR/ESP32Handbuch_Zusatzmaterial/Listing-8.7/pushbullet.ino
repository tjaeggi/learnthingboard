#include <WiFiClientSecure.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>        // notwendig für exakte Druckberechnung
#include <Adafruit_BMP280.h>

WiFiClientSecure secureClient;      // Client-Objekt erstellen

const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
const char* pushBulletAPIKEY = "o……        ..Y";  

const uint16_t pbTimeout = 3000;    // Wartezeit auf Antwort in Millisekunden 
const char*    pbHost = "api.pushbullet.com";

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt

// Push-Nachricht senden
bool pushbulletSendPush(const String &messageBody) {       
  uint32_t sendezeit = millis();                 
  String message = R"({"type": "note", "title": "Alarm vom ESP32", "body": ")" + messageBody + R"("})";
      
  if (!secureClient.connect(pbHost, 443)) {
    Serial.println("Pushbullet-Verbindung fehlgeschlagen!");
    return false;
  } else {       
    secureClient.print ("POST /v2/pushes HTTP/1.1\r\n");
    secureClient.printf("Host: %s\r\n", pbHost);
    secureClient.printf("Authorization: Bearer %s\r\n",  pushBulletAPIKEY);
    secureClient.printf("Content-Type: application/json\r\n");
    secureClient.printf("Content-Length: %d\r\n", message.length());
    secureClient.printf("\r\n%s\r\n", message.c_str());
    Serial.println("POST gesendet, message: ");
    Serial.println(message);  
  }

  while (!secureClient.available()) {
    if (millis() - sendezeit > pbTimeout) {
      Serial.println("Pushbullet Client Timeout!");
      secureClient.stop();
      return false;
    }
  }

  // zeigt die Zeit bis zur Antwort; ggf. Timeout entsprechend anpassen
  Serial.printf("Pushbullet-Antwort nach: %4ld ms\n", millis() - sendezeit); 
  String str = "";                        // String für Pushbullet-Antwort

  while (secureClient.available()) { 
    char c = secureClient.read();         // ein Byte lesen                  
    str += c;                             // das Byte an String str anfügen  
  } 
  Serial.println (str);    
  secureClient.stop();    
  return true;
}


void setup() {
  Serial.begin(115200);
  delay(100);  
  Serial.print("Verbindungsaufbau zu ");   
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  }  
}

void loop() {
  float temperatur = bmp.readTemperature();
  Serial.println("Temperatur: " + String(temperatur) + " *C");
  if (temperatur > 22 ) {
    String message = "Die Temperatur beträgt " + String(temperatur) + " *C";
    bool ret = pushbulletSendPush (message); 
    delay (4000);                         // Abkühlen abwarten 
  }
  delay (1500);
}
