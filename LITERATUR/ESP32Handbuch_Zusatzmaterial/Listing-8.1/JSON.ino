#include <ArduinoJson.h>                    // JSON aufbereiten
String jsonString;

// Serialisieren eines JSON-Strings
void serialisieren() {
  Serial.println("serialisieren");    
  // DynamicJsonDocument 
  //  const size_t capacity = JSON_OBJECT_SIZE(6) + JSON_ARRAY_SIZE(3) + \
  //  JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(5);
  //  DynamicJsonDocument doc(capacity);
  // StaticJsonDocument               304 (String)
  StaticJsonDocument<304> doc;               
  doc["Name"] = "ESP32-WROOM";            // obj + 1  
  doc["Takt"] = 160;                      // obj + 1  
  doc["VCC"].set(3.3);                    // obj + 1   

  JsonObject Speicher = doc.createNestedObject("Speicher"); // obj + 1 
  Speicher["ROM"] = 448;                  // obj + 1  
  Speicher["SRAM-Data"] = 520;            // obj + 1  
  Speicher["RTC"] = 16;                   // obj + 1  

  JsonObject GPIO = doc.createNestedObject("GPIO");    // obj + 1
  JsonArray GPIO_PIN41 = GPIO.createNestedArray("PIN41"); // obj + 1
  GPIO_PIN41.add("GPIO01");               // arr + 1  
  GPIO_PIN41.add("CLK3");                 // arr + 1 
  GPIO_PIN41.add("U0_TXD");               // arr + 1 

  JsonArray GPIO_PIN13 = GPIO.createNestedArray("PIN13");  // obj + 1
  GPIO_PIN13.add("GPIO33");               // arr + 1  
  GPIO_PIN13.add("Touch9");               // arr + 1  
  GPIO_PIN13.add("RTC8");                 // arr + 1 
  GPIO_PIN13.add("ADC1_5");               // arr + 1 
  GPIO_PIN13.add("XTAL32");               // arr + 1 

  doc["Kamera"] = false;                  // arr + 0 

  serializeJson(doc, Serial);            // gibt JSON-Dokument aus
  Serial.println (); 
  serializeJson(doc, jsonString);
  Serial.println (); 
  return;
}

// Deserialisieren eines JSON-Strings
void deserialisieren() {
  Serial.println("deserialisieren");  
  // DynamicJsonDocument   
  //const size_t capacity = JSON_OBJECT_SIZE(6) + JSON_ARRAY_SIZE(3) + \
  //  JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(5);
  //DynamicJsonDocument doc(capacity);
  // StaticJsonDocument  
  StaticJsonDocument<433> doc;  // 304 (String) + 129 (Duplizieren)
  
  deserializeJson(doc, jsonString);
  auto err = deserializeJson(doc, jsonString);
  if (err) {         
    Serial.print  ("Fehler JSON-deserialize  ");
    Serial.println(err.c_str());
  } else {   
    const char* name = doc["Name"]; 
    int takt = doc["Takt"]; 
    float vcc = doc["VCC"];
    int rom = doc["Speicher"]["ROM"];
    const char* gpio41TX = doc["GPIO"]["PIN41"][2];
    bool kamera = doc["Kamera"];
    Serial.println("Name:             " + String (name) );  
    Serial.println("Takt:             " + String (takt) ); 
    Serial.println("VCC:              " + String (vcc) ); 
    Serial.println("Speicher / ROM:   " + String (rom) ); 
    Serial.println("GPIO / PIN41 / 2: " + String (gpio41TX) ); 
    Serial.println("Kamera:           " + String (kamera) ); 
  }    
  Serial.println();  
}

void setup() {
  Serial.begin(115200);  
  serialisieren();     
  deserialisieren();     
}

void loop() {   
  delay(1000);
}   
