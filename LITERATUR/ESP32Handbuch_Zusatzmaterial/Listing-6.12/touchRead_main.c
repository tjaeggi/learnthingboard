#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/touch_pad.h"
#define PROZENT_SCHWELLENWERT          (80)
#define TOUCHPAD_FILTER_TOUCH_PERIOD   (10)
#define TOUCH_PIN                      0     // touch 0 = GPIO4
#define LED_PIN                        2     // GPIO 2

static uint16_t erstWert;

static void setupTouch(void) {
  touch_pad_init();
  touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
  touch_pad_config(TOUCH_PIN, 0);	
  touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD);
  touch_pad_read_filtered(TOUCH_PIN, &erstWert);	
  printf("testlesen Touchpin %d freilaufend - ", TOUCH_PIN); 	
  printf("gelesener Wert %d \n", erstWert);	
  return;
}

static void lesenTouch() {
  uint16_t aktuellWert = 0;
  touch_pad_read_filtered(TOUCH_PIN, &aktuellWert);
  if (aktuellWert < erstWert * PROZENT_SCHWELLENWERT / 100) {
    printf("Touch registriert Touchpin %d - ", TOUCH_PIN); 
    printf("Wert bei init %d, nun gelesen %d \n", erstWert, aktuellWert);
    gpio_set_level(LED_PIN, 1);
    vTaskDelay(1000 / portTICK_PERIOD_MS);        
  } else {
    gpio_set_level(LED_PIN, 0);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }  
}

void app_main() {
  printf("Start des setup\n");
  setupTouch();	
  gpio_pad_select_gpio(LED_PIN);
  gpio_set_direction(LED_PIN, GPIO_MODE_OUTPUT);	
  gpio_set_level(LED_PIN, 0);	
  printf("Ende des setup\n");	
  while(1) {
    lesenTouch();
  }
}
