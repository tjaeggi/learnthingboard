#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "thermometer.h"

#define SCREEN_WIDTH 128     // OLED-Display-Breite in Pixel
#define SCREEN_HEIGHT 64     // OLED-Display-Höhe in Pixel

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

void zeichneOled(void) {
  display.clearDisplay();             // Puffer löschen    
  display.drawBitmap(
    0, 6, thermometer_bmp, THERMOMETER_WIDTH, THERMOMETER_HEIGHT, WHITE);
  display.display(); 
  delay(4000);
}

void setup() {
  Serial.begin(115200);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println("SSD1306-Beginn fehlgeschlagen");
    for(;;); 
  } 
}

void loop() {
  zeichneOled();   
  delay(4000);
}
