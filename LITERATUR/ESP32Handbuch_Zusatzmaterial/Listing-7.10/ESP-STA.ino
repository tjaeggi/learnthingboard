
#include <WiFi.h>                           // für WiFi 

#define LED_PIN    2                        // GPIO2, PIN G2
// Definition der Zugangsdaten für den Router/Access-Point
const char* ssid     = "ssidRouter";
const char* password = "passwortRouter";

WiFiServer serverWiFi (80);                 // Port für Webserver

// sendet eine Antwort an den WiFi-Client
void wifiSend (WiFiClient client) {  
  // Betriebszeit aufbereiten
  int sekunden = (millis()/1000) % 60;
  int minuten = (millis()/(1000*60)) % 60;
  int stunden = (millis()/(1000*60*60)) % 24;  
  
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html"); 
  client.println();  
  // der Inhalt der HTTP-Antwort folgt auf den HTTP-Header
  client.print ("<!DOCTYPE HTML>");
  client.print("Betriebszeit: " + String(stunden) + ":" + String(minuten) +
                             ":" + String(sekunden) + "<br>");
  client.print("Klick <a href=\"/H\">an</a> oder <a href=\"/L\">aus</a> !");
  client.print("</html>");
  // die HTTP-Antwort endet mit einer weiteren Leerzeile
  client.println();    
}


// bearbeitet eine Anfrage von dem WiFi-Client
void wifiReceive (WiFiClient client) {
  Serial.println("Neue Anfrage.");          // Meldung im seriellen Monitor
  String currentLine = "";                  // Variable f. eingehende Daten
  while (client.connected()) {              // Loop, solange Client verbunden
    if (client.available()) {               // gibt es Bytes zu lesen   
      char c = client.read();               // 1 Byte in Variable c lesen
      Serial.print(c);                      // Ausgabe des Bytes 
      if (c == '\n') {                      // ist Zeichen newline-Zeichen ?
        // das Ende der HTTP-Anfrage ist eine Leerzeile 
        // und zwei newline-Zeichen hintereinander
        if (currentLine.length() == 0) {    // Ende HTTP-Anforderung Client        
          wifiSend(client);            
          break;                            // while beenden client.connected 
        } else {                            // liegt ein newline vor
         currentLine = "";                  // Variable currentLine löschen
        }
      } else if (c != '\r') {               // alles, nur kein Wagenrücklauf
        currentLine += c;                   // Zeichen currentLine hinzufügen
      }
      // Auswertung der Clientanfrage     
      // war die Clientanfrage "GET /H" oder "GET /L":
      if (currentLine.endsWith("GET /H")) {
        digitalWrite(LED_PIN, HIGH);        // GET /H schaltet die LED an
      }
      if (currentLine.endsWith("GET /L")) {
        digitalWrite(LED_PIN, LOW);         // GET /L schaltet die LED aus     
      }  
    }                                       // end if client.available      
  }                                         // end while client.available 
  
  client.stop();                            // Verbindung schließen
  Serial.println("Client Disconnected.");  
  Serial.println();      
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);   
  Serial.print("Verbindungsaufbau zu ");    // Verbindung zum WiFi-Netzwerk
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    }  
  Serial.println("");                       // Verbindung aufgebaut
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();  
  Serial.println("Start WiFi Server!");  
  serverWiFi.begin();

}


void loop() {
  WiFiClient client = serverWiFi.available();  // horcht auf Client-Anfragen
  if (client) {                             // fragt ein Client an?
    wifiReceive (client);                   // Anfrage aufbereiten
  }      
  delay(1);
}