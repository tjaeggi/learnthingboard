#include <WiFi.h>
#include <Adafruit_Sensor.h>        
#include <Adafruit_BMP280.h>

WiFiClient client;

const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
const char* iftttKey = "IFTTT-Key";      // Key von IFTTT

const uint16_t pbTimeout = 3000;    // Wartezeit auf Antwort in Millisekunden 
const char*    iftttHost = "maker.ifttt.com";

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
WiFiServer Webserver(80);

 // IFTTT-Nachricht senden
bool iftttSend() {
  uint32_t sendezeit = millis(); 

  if (!client.connect(iftttHost, 80)) {
    Serial.println("IFTTT-Verbindung fehlgeschlagen!");
    return false;
  } else {     
    client.print("POST /trigger/");
    client.print("ESP32");             // das definierte Trigger-Event
    client.print("/with/key/");
    client.print(iftttKey);
    client.println(" HTTP/1.1");
    client.printf("Host: %s\r\n", iftttHost);
    client.println("User-Agent: ESP32");
    client.println("Connection: close");
    client.println();
    Serial.println("POST gesendet !");
  }

  while (!client.available()) {
    if (millis() - sendezeit > 5000) {
      Serial.println("IFTTT-Client Timeout !");
      client.stop();
      return false;
    }
  }                       

  String str;        
  while (client.available()) { 
    char c = client.read();         // ein Byte lesen                  
    str += c;                       // das Byte an String str anfügen  
  } 
  Serial.println (str);    
  client.stop();    
  return true;
}

void setup() {
  Serial.begin(115200);
  delay(100);  
  Serial.print("Verbindungsaufbau zu ");   
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  }  
  
  Webserver.begin();
}

void loop() {
  float temperatur = bmp.readTemperature();
  Serial.println("Temperatur: " + String(temperatur) + " *C");
  if (temperatur > 22 ) {
    bool ret = iftttSend (); 
    delay (4000);                         // Abkühlen abwarten 
  }
  delay (3000);
}
