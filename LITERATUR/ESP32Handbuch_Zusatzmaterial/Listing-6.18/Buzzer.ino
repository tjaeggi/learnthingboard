#define LEDC_KANAL_0       0         // Kanal 0; einer der 16-LEDC-Kanäle
#define LEDC_AUFLOESUNG    8         // 8-Bit-Tastverhältnisauflösung (0-255)
#define LEDC_FREQ          5000      // 5.000 Hz Frequenz
#define BUZZER_PIN         4         // GPIO4

// 13 Noten bestehend aus Ton, Oktave, Dauer
typedef struct {note_t ton; uint8_t oktave; int laenge;} note; 
note melodie [13] = { 
  {NOTE_F, 3, 500}, {NOTE_D, 3, 500}, {NOTE_D, 3, 1000},
  {NOTE_E, 3, 500}, {NOTE_C, 3, 500}, {NOTE_C, 3, 1000},
  {NOTE_B, 2, 500}, {NOTE_C, 3, 500}, {NOTE_D, 3, 500}, {NOTE_E, 3, 500},
  {NOTE_F, 3, 500}, {NOTE_F, 3, 500}, {NOTE_F, 3, 1000} };     

void buzzer() {
  for (int i = 0; i < 13; i++) {
    ledcWriteNote(LEDC_KANAL_0, melodie[i].ton, melodie[i].oktave); 
    delay (melodie[i].laenge);  
    ledcWrite(LEDC_KANAL_0, 0);           
    delay (30);  
  } 
  ledcWrite(LEDC_KANAL_0, 0);           // Tastverhältnis 0 % 
  delay (1000);                         // Pause zwischen zwei Noten
}

void setup() {
  Serial.begin(115200);  
  ledcSetup(LEDC_KANAL_0, LEDC_FREQ, LEDC_AUFLOESUNG);
  ledcAttachPin(BUZZER_PIN, LEDC_KANAL_0);
}

void loop() {
  buzzer();  
}
