#define TOUTCH_PIN1 T3                  // ESP32 Pin D0
#define TOUTCH_PIN2 T0                  // ESP32 Pin D4
#define LED_PIN 2
#define LED_PIN1 12
#define LED_PIN2 14
#define LED_PIN3 27
#define LED_PIN4 25
#define LED_PIN5 33
#define LED_PIN6 32
#define LED_PIN7 26

unsigned long zeit1, zeit1Diff;         // Dauer Touch 1 berührt
unsigned long zeit2, zeit2Diff;         // Dauer Touch 2 berührt 
unsigned long zeit3, zeit3Diff;         // Dauer Touch für Würfeln
int touchValue1, touchValue2;
int maxS = 30;

// LEDs ausschalten
void setLedLow() {
  digitalWrite (LED_PIN1, LOW);
  digitalWrite (LED_PIN2, LOW);
  digitalWrite (LED_PIN3, LOW);
  digitalWrite (LED_PIN4, LOW);
  digitalWrite (LED_PIN5, LOW);
  digitalWrite (LED_PIN6, LOW);
  digitalWrite (LED_PIN7, LOW);
  return;     
}

// Ausgeben der gewürfelten Zahl
void displayZahl (int zahl) {
  switch (zahl) {
    case 1: digitalWrite (LED_PIN7, HIGH);   
            break;
    case 2: digitalWrite (LED_PIN2, HIGH);   
            digitalWrite (LED_PIN5, HIGH);     
            break; 
    case 3: digitalWrite (LED_PIN1, HIGH);   
            digitalWrite (LED_PIN7, HIGH);   
            digitalWrite (LED_PIN6, HIGH);    
            break;               
    case 4: digitalWrite (LED_PIN1, HIGH);   
            digitalWrite (LED_PIN3, HIGH); 
            digitalWrite (LED_PIN4, HIGH); 
            digitalWrite (LED_PIN6, HIGH);   
            break;                               
    case 5: digitalWrite (LED_PIN1, HIGH);    
            digitalWrite (LED_PIN3, HIGH); 
            digitalWrite (LED_PIN7, HIGH); 
            digitalWrite (LED_PIN4, HIGH); 
            digitalWrite (LED_PIN6, HIGH);   
            break;                            
    case 6: digitalWrite (LED_PIN1, HIGH);   
            digitalWrite (LED_PIN2, HIGH);   
            digitalWrite (LED_PIN3, HIGH); 
            digitalWrite (LED_PIN4, HIGH); 
            digitalWrite (LED_PIN5, HIGH); 
            digitalWrite (LED_PIN6, HIGH); 
            break;                       
  }
  return;  
}

void wuerfeln (int art) {
  int zahl;
  for (int i = 0; i < 30; i++) {        // Rollen des Würfels simulieren 
    zahl = random (1, 6);
    displayZahl(zahl);    
    delay(15 * i);                      // verlangsamen
    setLedLow();
  }
  if (art == 1)      zahl = 1;
  else if (art == 6) zahl = 6;  
  displayZahl(zahl);   
  delay (1000);
  setLedLow();
  return;
}

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(LED_PIN1, OUTPUT);
  pinMode(LED_PIN2, OUTPUT);    
  pinMode(LED_PIN3, OUTPUT);
  pinMode(LED_PIN4, OUTPUT);
  pinMode(LED_PIN5, OUTPUT);
  pinMode(LED_PIN6, OUTPUT);        
  pinMode(LED_PIN7, OUTPUT);  
  digitalWrite (LED_PIN, LOW);
  setLedLow();   
  zeit1 = micros();
  zeit2 = micros();
  zeit3 = micros();
}

void loop() {
  touchValue1 = touchRead(TOUTCH_PIN1);
  touchValue2 = touchRead(TOUTCH_PIN2);  

  if (touchValue1 < maxS ||
      touchValue2 < maxS) {
    if (touchValue1 > 5 && touchValue1 < maxS) {
      zeit1Diff = micros() - zeit1;
    } 
    if (touchValue2 > 5 && touchValue2 < maxS) {
      zeit2Diff = micros() - zeit2;
    }
     
    int antT1, antT2;
    zeit3Diff = micros() - zeit3;
    if (zeit3Diff > 500000) {
      antT1 = zeit1Diff * 100 / (zeit1Diff+zeit2Diff);  // %-Anteil
      antT2 = zeit2Diff * 100 / (zeit1Diff+zeit2Diff);  // %-Anteil
      if (antT1 > 75 || antT2 > 75) {   // nur ein Touch angetippt 
        wuerfeln (0);                  
      } else 
      if (antT1 < 35) {                 // zuerst Tip T1
        wuerfeln (1); 
      } else 
      if (antT2 < 35) {                 // zuerst Tip T2           
        wuerfeln (6);   
      } else {                          // Tip nicht eindeutig
        wuerfeln (0);
      }
      delay (1000);
      zeit1 = micros();
      zeit2 = micros();       
      zeit3 = micros();
      zeit1Diff = 0;
      zeit2Diff = 0;
    }       
    digitalWrite (LED_PIN, HIGH);
  } else {
    zeit1 = micros();
    zeit2 = micros();    
    zeit3 = micros();
    zeit1Diff = 0;
    zeit2Diff = 0;
    digitalWrite (LED_PIN, LOW);
  } 
  delay(10);
}  
