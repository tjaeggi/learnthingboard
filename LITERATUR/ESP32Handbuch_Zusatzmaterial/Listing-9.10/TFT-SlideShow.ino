#include "SPI.h"                   // für SPI
#include <Adafruit_GFX.h>          // Core Graphics Library
#include <Adafruit_ST7735.h>       // Hardware-specific Library for ST7735
#include "SD.h"                    // Methoden SD-Karte

#define TFT_CS        12           //  Hallowing display control pins: chip select
#define TFT_RST       33           //  Display reset
#define TFT_DC        27           //  Display data/command select
#define TFT_SCLK      14           //  Clock gelb
#define TFT_MOSI      13           //  MOSI blau

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);


void zeichneTFT(String dateiName) {
  
  typedef struct WinbmpDateiHeader {
    uint16_t  FileType;        // File type, always 4D42h ("BM") 
    uint32_t  FileSize;        // Size of the file in bytes 
    uint16_t  Reserved1;       // Always 0 
    uint16_t  Reserved2;       // Always 0 
    uint32_t  BitmapOffset;    // Starting position of image data in bytes 
  } bmpFH;
  bmpFH bmpFH1;

  typedef struct Win4xBitmapHeader {  // Version 4  Windows 95
    uint32_t  Size;            // Size of this header in bytes 
    long      Width;           // Image width in pixels 
    long      Height;          // Image height in pixels 
    uint16_t  Planes;          // Number of color planes 
    uint16_t  BitsPerPixel;    // Number of bits per pixel 
    uint32_t  Compression;     // Compression methods used 
    uint32_t  SizeOfBitmap;    // Size of bitmap in bytes 
    long      HorzResolution;  // Horizontal resolution in pixels per meter 
    long      VertResolution;  // Vertical resolution in pixels per meter 
    uint32_t  ColorsUsed;      // Number of colors in the image 
    uint32_t  ColorsImportant; // Minimum number of important colors 
    // Fields added for Windows 4.x follow this line 
    uint32_t  RedMask;         // Mask identifying bits of red component 
    uint32_t  GreenMask;       // Mask identifying bits of green component 
    uint32_t  BlueMask;        // Mask identifying bits of blue component 
    uint32_t  AlphaMask;       // Mask identifying bits of alpha component 
    uint32_t  CSType;          // Color space type 
    long      RedX;            // X coordinate of red endpoint 
    long      RedY;            // Y coordinate of red endpoint 
    long      RedZ;            // Z coordinate of red endpoint 
    long      GreenX;          // X coordinate of green endpoint 
    long      GreenY;          // Y coordinate of green endpoint 
    long      GreenZ;          // Z coordinate of green endpoint 
    long      BlueX;           // X coordinate of blue endpoint 
    long      BlueY;           // Y coordinate of blue endpoint 
    long      BlueZ;           // Z coordinate of blue endpoint 
    uint32_t  GammaRed;        // Gamma red coordinate scale value 
    uint32_t  GammaGreen;      // Gamma green coordinate scale value 
    uint32_t  GammaBlue;       // Gamma blue coordinate scale value 
  } bmpBH;
  bmpBH bmpBH1;

  uint16_t pixel;
  File     bmpDatei;
  boolean  flip    = true;     // Normalfall; BMP-Daten aus Datei bottom-up
  int      w, h, row, col;
  uint32_t pos = 0, startTime = millis();

  bmpDatei = SD.open("/" + dateiName);
  if (!bmpDatei){
    Serial.println("lesendes Öffnen der Datei gescheitert");
    return;
  }
  
  // File-header auswerten
  bmpFH1.FileType = lese16Bit(bmpDatei);        // Typ = 2 Byte 

  if (bmpFH1.FileType == 0x4D42) {              // BMP-Signatur 
    bmpFH1.FileSize = lese32Bit(bmpDatei);      // Größe = 4 Byte  
    lese32Bit(bmpDatei);                        // 4 reservierte Bytes überspringen
    bmpFH1.BitmapOffset = lese32Bit(bmpDatei);  // Größe = 4 Byte 
      
    // Bitmap-Header auslesen
    bmpBH1.Size = lese32Bit(bmpDatei);          // Größe = 4 Byte    
    bmpBH1.Width = lese32Bit(bmpDatei);         // Größe = 4 Byte 
    bmpBH1.Height = lese32Bit(bmpDatei);        // Größe = 4 Byte    
    if ((bmpBH1.Width  > tft.width()) ||
        (bmpBH1.Height > tft.height()) ) {
      Serial.print  ("Bild-Größe größer als TFT-Display");  
      return;       
    }   
      
    bmpBH1.Planes = lese16Bit(bmpDatei);        // 2 Byte 
  
    if (bmpBH1.Planes == 1) {                   // muss 1 sein 
      bmpBH1.BitsPerPixel = lese16Bit(bmpDatei);  // 2 Byte      
      bmpBH1.Compression =lese32Bit(bmpDatei);  // 4 Byte 
     
      if ((bmpBH1.BitsPerPixel == 24) && (bmpBH1.Compression == 0)) { // 0 = uncompresse
        uint32_t fileRowSize = (bmpBH1.Width * 3 + 3) & ~3;      
        if(bmpBH1.Height < 0) {
          bmpBH1.Height = -bmpBH1.Height;
          flip      = false;
        }

        w = bmpBH1.Width;
        h = bmpBH1.Height;

        // Display-Reihe von oben nach unten
        // Datei-Reihen von unten nach oben (Normafall Height positiv)
        for (row=0; row<h; row++) {             // für jede Reihe
          if (flip) {                           // bmp bottom-up 
            pos = bmpFH1.BitmapOffset + (bmpBH1.Height - 1 - row) * fileRowSize;
          } else {                              // bmp top-down
            pos = bmpFH1.BitmapOffset + row * fileRowSize;  
          }        
          if (bmpDatei.position() != pos) {     // erneutes Positionieren         
            bmpDatei.seek(pos);
          }

          for (col=0; col<w; col++) {           // für jedes Pixel   
            unsigned b = bmpDatei.read();   
            unsigned g = bmpDatei.read();  
            unsigned r = bmpDatei.read();      
            pixel = ((r & 0xf8) << 8) + ((g & 0xfc) << 3) + (b >> 3);
            tft.drawPixel(col, row, pixel);        
          }                                     // Ende Pixel 
        }                                       // Ende Reihe
      } else {
        Serial.println("kein gültiges BMP-Format ");
      }                     // Ende bmp-Format (24 Pixel, Compression =  0
    }
  }

  bmpDatei.close();   
  return ;
}

// Lesen und Zurückgeben von 2 / 4 Byte aus der SD-Karten-Datei
uint16_t lese16Bit(File datei) {
  uint16_t ergebnis;
  ((uint8_t *)&ergebnis)[0] = datei.read(); 
  ((uint8_t *)&ergebnis)[1] = datei.read(); 
  return ergebnis;
}

uint32_t lese32Bit(File datei) {
  uint32_t ergebnis;
  ((uint8_t *)&ergebnis)[0] = datei.read(); 
  ((uint8_t *)&ergebnis)[1] = datei.read();
  ((uint8_t *)&ergebnis)[2] = datei.read();
  ((uint8_t *)&ergebnis)[3] = datei.read(); 
  return ergebnis;
}

void setup(){
  Serial.begin(115200);
  tft.initR(INITR_BLACKTAB);   
  delay(500);

  if (!SD.begin()){ 
    Serial.println("Mount der SD-Karte gescheitert");
    while(1){};                    // keine weitere Bearbeitung
  }
  
  if (SD.cardType() == CARD_NONE){
    Serial.println("Kartentyp nicht erkannt");
    while(1){};                    // keine weitere Bearbeitung
  } 
}

void loop(){
  File dir=SD.open("/");
  dir.rewindDirectory();
  while(true) {
    File datei =  dir.openNextFile();
    if (! datei) {
      Serial.println("keine weiteren Dateien mehr");
      Serial.println();
      break;
    }
    if (!datei.isDirectory()) {      // nur Dateien im Hauptverzeichnis
      Serial.print  ("Datei-name / größe: \t\t");
      Serial.print  (datei.name());
      Serial.print  ("  /  ");
      Serial.println(datei.size(), DEC);     
      String bmpDatei =  datei.name();
      if ( bmpDatei.endsWith (".bmp") ) {  
        tft.fillScreen(ST77XX_BLACK); 
        zeichneTFT(bmpDatei);
        Serial.println(); 
        delay (3000);                // Dauer der Anzeige
      }
    }
    datei.close();
  }
  delay (1000);                      // 1 Sek. Pause, alle Bilder erneut
}
