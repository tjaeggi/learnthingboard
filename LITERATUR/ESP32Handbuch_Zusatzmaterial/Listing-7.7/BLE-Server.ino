#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" 
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define LED_PIN       2                // GPIO2, Pin G2                         

BLECharacteristic *pCharacteristicTX;  // Characteristic senden an Client
bool deviceConnected = false;          // Kontrolle Pairing
int ledStatusAlt = 0;

// Server Callback
class MyServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    deviceConnected = true;
  };
  void onDisconnect(BLEServer* pServer) {
    deviceConnected = false;
  }
};

// Callbacks für den Daten-Empfang
class MyCallbacks: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic) {
    std::string rxValue = pCharacteristic->getValue();

    if (rxValue.length() > 0) {
       Serial.print("empfangene BLE-Daten: ");
       for (int i = 0; i < rxValue.length(); i++) {
         Serial.print(rxValue[i]);
       }
       Serial.println();
       if (rxValue.find("ein") != -1) { 
         digitalWrite(LED_PIN, HIGH);
       }
       else if (rxValue.find("aus") != -1) {
         digitalWrite(LED_PIN, LOW);
       }
    }                     // end if rxValue.length() > 0  
  }                       // end function
};                        // end class       

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  
  // BLE-Device erstellen
  BLEDevice::init("ESP32BLE"); 

  // BLE-Server erstellen
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // BLE-Service erstellen
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // BLE Characteristic Senden erstellen
  pCharacteristicTX = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_TX,
                      BLECharacteristic::PROPERTY_NOTIFY
                      );                     
  pCharacteristicTX->addDescriptor(new BLE2902());

  // BLE Characteristic Empfangen erstellen
  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                       CHARACTERISTIC_UUID_RX,
                                       BLECharacteristic::PROPERTY_WRITE
                                       );
  pCharacteristic->setCallbacks(new MyCallbacks());
  
  // BLE-Server starten
  pService->start();              
        
  // Anfragen nach Verbindungsaufbau starten 
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06); 
  pAdvertising->setMinPreferred(0x12);
  pServer->getAdvertising()->start(); 
  
  Serial.println("Warten auf Verbindungsnachricht durch einen BLE-Client!");
}

void loop() {
  long messWert;
  if (deviceConnected) {
    char txString[20]; 
    if (ledStatusAlt != digitalRead (LED_PIN)  ) {
      if (digitalRead (LED_PIN)) {
        sprintf(txString, "LED eingeschaltet");    
      } else {
        sprintf(txString, "LED ausgeschaltet");    
      }
      ledStatusAlt = digitalRead (LED_PIN);
    } else {
      messWert = millis() / 1000;
      sprintf(txString, "Zeitstempel %d", messWert);    // Wert konvertieren  
    }
      
    pCharacteristicTX->setValue(txString);      
    pCharacteristicTX->notify();          // senden 
      
  }
  delay(2000);
}
