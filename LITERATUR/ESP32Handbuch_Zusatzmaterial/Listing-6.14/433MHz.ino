#include "RCSwitch.h"                     // Einbinden von rc-switch
#define TX_PIN   23                       // Sender-Pin  (GPIO23 - Pin D23)
#define RX_PIN   22                       // Empfänger-Pin (GPIO22 - Pin D22)
RCSwitch mySwitch = RCSwitch();

void setup() {
  Serial.begin(115200);
  mySwitch.enableTransmit(TX_PIN);        // Senden ermöglichen
  mySwitch.enableReceive(RX_PIN);         // Empfangen ermöglichen
}

void loop() {
  if (Serial.available() > 0) { 
    float codeSwitch = Serial.parseFloat();  
    mySwitch.send(codeSwitch, 24);   
    //mySwitch.send(5260625, 24); 
    //mySwitch.send("010100000100010101010001");
    //mySwitch.switchOn("00110", "10000");
    //mySwitch.sendTriState("FF00F0FFFF0F");
  }
  if (mySwitch.available()) {
    int value = mySwitch.getReceivedValue();
    if (value == 0) {
      Serial.print("unbekanntes Protokoll");
    } else {
      Serial.print  ("Protocol: ");
      Serial.print  ( mySwitch.getReceivedProtocol() );
      Serial.print  ("   empfangener Code: ");
      Serial.println( mySwitch.getReceivedValue() );
      Serial.print  ("   Bitlänge: ");
      Serial.print  ( mySwitch.getReceivedBitlength() );
      Serial.print  ("   Pulsweite: ");
      Serial.println( mySwitch.getReceivedDelay() );
      Serial.println();
    }
    mySwitch.resetAvailable();
  } 
}
