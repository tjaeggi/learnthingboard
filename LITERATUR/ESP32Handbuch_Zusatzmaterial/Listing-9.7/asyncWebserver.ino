#include <WiFi.h>                           // für WLAN
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "SPIFFS.h"

#define LED_PIN    2                        // GPIO2, Pin G2

const char* ssid     = "ssidRouter";
const char* password = "passwortRouter";
AsyncWebServer serverWiFi(80);

String txtModul, txtEin, txtAus;

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Seite nicht gefunden");
}

String lesenMillis() {
  int sek = (millis()/1000) % 60;
  int min = (millis()/(1000*60)) % 60;
  int std = (millis()/(1000*60*60)) % 24;  
  return String (String (std)+":"+String(min)+":"+String(sek));
}

// Lesen der Text-Datei mit Beschriftungen
void lesenTextDatei() { 
  String str;
  int i = 0; 

  File file = SPIFFS.open("/fileUpload.txt");
  if(!file){
    Serial.println("Fehler: Text-Datei kann nicht geöffnet werden");
    return;
  }
   
  while(file.available()){       
    str += char(file.read());  
    if (str.endsWith(String('\n')) ) {
      str.remove((str.length()-1), 1);      // Zeilenvorschub löschen
      if (i == 0) {
        txtModul = str;
      } else {
        if (i == 1) {
          txtEin = str;
          txtEin.toUpperCase();              
        }
      }
      i++;
      str = "";       
    }      
  } 
  txtAus = str;
  txtAus.toUpperCase();
  str.remove((str.length()-1), 1);
  file.close();
}

// ersetzt Platzhalter Webserver
String ersetzenPlaHa(const String& var){   
  if(var == "MODUL"){   
    return txtModul;
  }
  
  if(var == "ZEIT"){   
    return lesenMillis();
  }
    
  if(var == "STATUS"){
    String ledStatus;    
    if(digitalRead(LED_PIN)){
      ledStatus = txtEin;
    } else {
      ledStatus = txtAus;
    }
    return ledStatus;
  }
  if(var == "COLOUR"){
    String str;
    if(digitalRead(LED_PIN)){
      str = "redButton"; 
    } else {
      str = "greenButton";
    }   
    return str;    
  }  

  if(var == "STATUSNEU"){
    if(digitalRead(LED_PIN)){
      return txtAus;
    } else {
      return txtEin;
    }
  }  
  return String();                // Fehler => leerer String
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);   
  Serial.print("Verbindungsaufbau zu ");    
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);  
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }  
  Serial.println("");                       // Verbindung aufgebaut
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();  
  Serial.println("Start WiFi Server!");  
  serverWiFi.begin();

  if(!SPIFFS.begin(true)){
    Serial.println("Fehler: SPIFFS kann nicht gemountet werden");
    return;
  }
  lesenTextDatei();

  serverWiFi.onNotFound(notFound);

  // Methode für root / web page
  serverWiFi.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, ersetzenPlaHa);
  });
   
  // Methode für Laden der style.css-Datei
  serverWiFi.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });
  // Methode für Laden der jquery-3.4.1.min.js-Datei
  serverWiFi.on("/jquery-3.4.1.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/jquery-3.4.1.min.js", "text/javascript");
  });  
  // Callback für Laden der java.js-Datei
  serverWiFi.on("/java.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/java.js", "text/javascript");
  });      
 
  // Methode für das Schalten 
  serverWiFi.on("/switch", HTTP_GET, [](AsyncWebServerRequest *request){
    if(digitalRead(LED_PIN)){
      digitalWrite(LED_PIN, LOW); 
    } else {
      digitalWrite(LED_PIN, HIGH); 
    }   
    request->send(SPIFFS, "/index.html", String(), false, ersetzenPlaHa);   
  });

  // Methode für Update Betriebszeit
  serverWiFi.on("/updat", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send_P(200, "text/plain", lesenMillis().c_str());
  });
}

void loop() {
  delay(1);
}
