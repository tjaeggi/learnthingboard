#include <SPI.h>
#include <Adafruit_GFX.h>          // Core Graphics Library
#include <Adafruit_ST7735.h>       // Hardware-specific Library for ST7735
#include "victory.h"               // .h-Datei im Sketchordner

#define TFT_CS        12           //  Chip Select
#define TFT_RST       33           //  Display Reset
#define TFT_DC        27           //  Display Data/Command Select
#define TFT_SCLK      14           //  Clock gelb
#define TFT_MOSI      13           //  MOSI blau

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

void zeichneTFT() {
  tft.fillScreen(ST77XX_BLACK);
  tft.setCursor(0, 30);
  tft.setTextColor(ST77XX_MAGENTA);
  tft.setTextSize(2);
  tft.println("Victory !!");
  delay (2000);

  tft.fillScreen(ST77XX_BLACK);  
  tft.drawBitmap(0,0, victory, victory_width, victory_height, ST7735_WHITE);
  delay (3000);  
  
  tft.invertDisplay(true);
  delay(1500);
  tft.invertDisplay(false);
  delay(1500);    
}    

void setup(void) {
  tft.initR(INITR_BLACKTAB);     
  delay(500);
}

void loop() {
  zeichneTFT();
}
