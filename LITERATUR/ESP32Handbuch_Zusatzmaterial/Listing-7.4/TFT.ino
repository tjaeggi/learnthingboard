#include <SPI.h>
#include <Adafruit_GFX.h>          // für Zeichnen-Methoden
#include <Adafruit_ST7735.h>       // ST7735-Treiber

#define TFT_CS        12           //  Chip Select
#define TFT_RST       33           //  Display Reset
#define TFT_DC        27           //  Display Data/Command Select
#define TFT_SCLK      14           //  Clock gelb
#define TFT_MOSI      13           //  MOSI blau

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);

void zeichneTFT() {
  tft.fillScreen(ST77XX_BLACK);    // TFT-Inhalt löschen
  //tft.setTextWrap(false);
  tft.setCursor(0, 30);            // Cursor positionieren
  tft.setTextColor(ST77XX_WHITE);  // Textfarbe weiß
  tft.setTextSize(1);
  tft.println("Hallo ESP32");
  tft.setTextColor(ST77XX_GREEN);  // Textfarbe grün
  tft.setTextSize(3);              // 3-fache Textgröße
  tft.println("Hallo");
  delay(3000);
     
  tft.fillScreen(ST77XX_BLACK);
  tft.drawRect(10, 10, 30, 60, ST77XX_BLUE);
  delay(3000);
  
  tft.fillScreen(ST77XX_BLACK);
  for (int16_t x=tft.width()-1; x > 6; x-=12) {
    tft.fillRect(tft.width()/2 -x/2, tft.height()/2 -x/2 , x, x, ST77XX_BLUE);
    delay(20);
    tft.drawRect(tft.width()/2 -x/2, tft.height()/2 -x/2 , x, x, ST77XX_WHITE);
    delay(20);
  }
  delay(3000); 
  
  tft.fillScreen(ST77XX_BLACK);
  tft.fillCircle(40, 40, 30, ST77XX_CYAN);
  delay (3000);  
  tft.invertDisplay(true);
  delay(1500);
  tft.invertDisplay(false);
  delay(1500);  
}

void setup(void) {
  tft.initR(INITR_BLACKTAB);      
  delay(500);
}

void loop() {
  zeichneTFT();
}
