#define RXD2 16
#define TXD2 17
 
void setup() {
  Serial.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);
  Serial.println("Serial  Txd ist an Pin: "+String(TX));
  Serial.println("Serial  Rxd ist an Pin: "+String(RX)); 
  Serial.println("Serial2 Txd ist an Pin: "+String(RXD2));
  Serial.println("Serial2 Rxd ist an Pin: "+String(TXD2)); 
}

void loop() { 
  while (Serial.available()) {
    Serial2.print(char(Serial.read()));
  }
}
