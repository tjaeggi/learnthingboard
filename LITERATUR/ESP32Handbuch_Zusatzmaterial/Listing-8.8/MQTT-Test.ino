// vgl. Arduino-IDE :  BEISPIELE • PUBSUBCLIENT • MQTT_ESP8266.
#include <WiFi.h>                         
#include "PubSubClient.h"

const char* ssid     = " ssidRouter ";
const char* password = " passwortRouter ";
WiFiClient wifiClient;

// MQTT-Broker-Daten
const char* mqttServer = "broker.mqttdashboard.com";
PubSubClient mqttClient(wifiClient);

long zeitAlt = -9999;

// Callback Nachrichten-Empfang (subscribe)
void callback(char* topic, byte* message, unsigned int length) {
  String str;  
  for (int i = 0; i < length; i++) {
    str += (char)message[i];
  }
  Serial.print  ("Nachrichtenempfang für topic: ");
  Serial.print  (topic);
  Serial.print  (". Nachricht: ");  
  Serial.println(str);
}

// Verbindung zum MQTT-Server herstellen
void mqttConnect() {
  Serial.print  ("Verbindungsaufbau zum MQTT-Server ");  
  Serial.print  (mqttServer);
  while (!mqttClient.connected()) {
    Serial.print("."); 
    if (mqttClient.connect("ESP32Client")) {
      Serial.println();
      Serial.println("MQTT verbunden");
    } else {
      Serial.print("fehlgeschlagen, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" erneuter Versuch in 5 Sekunden");
      delay(5000);                     
    }
  }
  mqttClient.subscribe("test/esp32");
}

void setup() { 
  Serial.begin(115200);
  Serial.println();
  Serial.print("Verbindungsaufbau zu ");    
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }
  Serial.println("");  
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Verbindung zum MQTT-Server herstellen
  mqttClient.setServer(mqttServer, 1883); 
  mqttClient.setCallback(callback);  
  mqttConnect();
}

void loop() {
  if (!mqttClient.connected()) {
    mqttConnect();
  }
  mqttClient.loop();

  if (millis() - zeitAlt > 3000) {
    char txString[20];   
    zeitAlt = millis();
    sprintf(txString, "Zeitstempel %d", millis() / 1000); 
    Serial.println(String("Versand MQTT-Nachricht: ") + txString);
    mqttClient.publish("test/esp32", txString);
  } 
  delay(200);
}
