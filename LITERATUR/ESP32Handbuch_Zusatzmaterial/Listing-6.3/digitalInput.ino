#define INPUT_PIN   19                  // GPIO19 für digitalRead
int i = 0, statusIn = LOW, z = 0;
int statusSchalter = LOW;               // für Debounce
int statusSchalterAlt = LOW;  
unsigned long letzteDebounceZeit = 0, debounceDelay = 60;
void setup() {
  Serial.begin (115200);
  delay (2);
  pinMode(INPUT_PIN, INPUT_PULLUP);
}
void loop() {
  statusIn = digitalRead (INPUT_PIN);
  //if (statusIn == LOW)    z++;        // ohne Debounce-Filter
  if (statusIn != statusSchalterAlt) {  // Beginn Debounce-Filter
    letzteDebounceZeit = millis();
  }
  
  if ((millis() - letzteDebounceZeit) > debounceDelay) {
    if (statusIn != statusSchalter) {  
      statusSchalter = statusIn;
      z++;
    }
  }                                      // Ende Debounce-Filter
  i++;
  if (i > 1000) {
    i = 0;
    Serial.println ("Zähler: " + String(z));  
  } 
  statusSchalterAlt = statusIn; 
  delay (1); 
}
