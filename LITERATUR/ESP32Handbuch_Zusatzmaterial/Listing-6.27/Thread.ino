#include <pthread.h>      

struct daten {
  int wert;
  char meld [60];
};

int ergMult (int in) {
  return in * 1111;
}

pthread_t pthr;               // Thread Identifier
struct daten *f = NULL;
int i = 0;
int *ptr;                     // Pointer-Deklaration

void *threadFunc1 (void *arg) { 
  struct daten *f = (struct daten *) arg;
  int i = 1;
  static int retour;          // statische oder globale Variable!!
  Serial.println("start threadFunc1 mit dem Parameter: " + String(f->meld));      
  Serial.println("threadFunc1 hat die ID             : " + String(pthread_self()) );        
  while (i < 8) {
    delay(1000);   
    Serial.println(String(i) + ". Durchlauf threadFunc1");      
    i++;
  }
  retour = ergMult(i);
  f->wert = i;
  Serial.println("Thread meldet: Ende threadFunc1 mit der Rückgabe:   " + String (retour)); 
  pthread_exit (&retour);     // gibt Adresse von retour zurück
}

void setup () {
  Serial.begin (115200); 
  // Speicher für Daten anfordern
  f = (struct daten*) malloc (sizeof (struct daten));
  
  if (f == NULL) {
    Serial.println ("Konnte keinen Speicher reservieren ...!");
    delay (5000);
  }
  
  Serial.println ("Speicher reserviert!");
  strcpy (f->meld , "Hallo Thread 1");
} 

void loop() { 
  if (pthread_create (&pthr, NULL, threadFunc1, f) != 0) {        
    Serial.println ("Thread konnte nicht erstellt werden!!");
  }

  i = 1;
  while (i < 3) {
    delay(1500);      
    Serial.println ("while in loop läuft " + String (i) + ". mal"); 
    i++;
  }
  Serial.println("loop wartet auf das Ende des Threads ... " );
  // weil ein Pointer zurückgegeben wird, muss mit einem doppelten
  // Pointer gearbeitet werden
  pthread_join (pthr, (void**) &ptr);   // &ptr = Adresse von ptr
  Serial.print  ("loop meldet: Thread beendet Wert :" + String(f->wert) ); 
  Serial.println(" erhaltener Rückgabewert " + String(*ptr) );
  Serial.println();
  Serial.println();
  delay (5000);
} 
