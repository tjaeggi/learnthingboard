#include <WiFi.h>
#include <Wire.h>
#include "PubSubClient.h"                 // für MQTT
#include <Adafruit_Sensor.h>        
#include <Adafruit_BMP280.h>
#include <ArduinoJson.h>                  // JSON aufbereiten

#define LED_PIN    2                      // GPIO2, Pin G2
            
const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
WiFiClient wifiClient;                    // WiFi-Objekt

const char* mqttServer = "192.168.2.215";
const char* mqttClientName = "ES32Client";
PubSubClient mqttClient(wifiClient); 

Adafruit_BMP280 bmp;                      // I2C-BMP280-Objekt
long millisAlt = -999999;


// Callback für eingehende Nachrichten
void callback(char* topic, byte* message, unsigned int length) {
  String str;   
  for (int i = 0; i < length; i++) {
    str += (char)message[i];
  }
  Serial.print  ("Nachrichtenempfang für topic: ");
  Serial.print  (topic);
  Serial.print  (". Nachricht: ");  
  Serial.println(str);
  if(str == "on"){
    digitalWrite(LED_PIN, HIGH);
  }
  else if(str == "off"){
    digitalWrite(LED_PIN, LOW);
  }
}

// MQTT-Verbindung zu Mosquitto herstellen
void mqttConnect() {
  int8_t ret;
  if (mqttClient.connected()) {           // return bei bestehender Verbindung
    return;
  }

  Serial.println("MQTT-Verbindungsaufbau");
  int versuche = 3;
  while (!mqttClient.connected()) {
    //if (mqttClient.connect(mqttClientName, mqttUser, mqttPassword )) {      
    if (mqttClient.connect(mqttClientName)) {
      Serial.println("MQTT verbunden");
    } else {
      Serial.print  ("Fehler Verbindungsaufbau, Code: ");
      Serial.print  (mqttClient.state());  
      Serial.println(" - erneuter Versuch in 5 Sekunden");
      mqttClient.disconnect();
      delay(5000);                    // 5 Sekunden warten
      versuche--;
      if (versuche == 0) {
        while (1);
      }      
    }
  }  
  mqttClient.subscribe("esp32/LED");
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN,OUTPUT);

  Serial.print("Verbindungsaufbau zu ");   
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Verbindungsdaten zum MQTT-Server
  mqttClient.setServer(mqttServer, 1883); 
  mqttClient.setCallback(callback);    
  mqttConnect();  

  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  }  

}

void loop() {
  if (!mqttClient.connected()) {
    mqttConnect();
  }
  mqttClient.loop();  

  if (millis() - millisAlt > 30000) {   // alle 30 Sekunden
    millisAlt = millis();
    float temperatur = bmp.readTemperature();
    float luftdruck = bmp.readPressure() / 100; 
    StaticJsonDocument<44> doc;               
    doc["temperatur"] = temperatur;  
    doc["luftdruck"] = luftdruck;                        
    String jsonString;
    serializeJson(doc, jsonString);            // gibt JSON-Dokument aus
    Serial.println( jsonString);
    char str [80];
    strcpy (str, jsonString.c_str() );   
    Serial.print   ("Senden von Temperatur: " + String(temperatur) );
    Serial.println ("  und Luftdruck: " + String(luftdruck) );
    const char* BMP280Topic ="esp32/bmp280";     
    mqttClient.publish(BMP280Topic, str);   
  }
  
  delay (10);
}
