#include "BluetoothSerial.h"              // Serial Bluetooth
#define LED_PIN     2                     // GPIO2, Pin G2
BluetoothSerial ESP_BT;                   // Bluetooth-Instanz

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);   
  ESP_BT.begin("ESP32");                  // Name des Bluetooth-Geräts
  Serial.println("Bluetooth-Gerät bereit für das Pairing");
}

void loop() {
  String zeile = "";                      // für die eingegangenen Zeichen
    
  if (ESP_BT.available()) {               // gibt es neue BT-Nachrichten?
    while (ESP_BT.available()) {  
      char c  = ESP_BT.read();            // Lesen der anstehenden Daten
      zeile += c;      
    }
    Serial.println("empfangene Bluetooth-Classic-Daten: "); 
    Serial.println(zeile);  
    if (zeile.startsWith("ein") ) {       
      digitalWrite(LED_PIN, HIGH);
      ESP_BT.println("LED eingeschaltet");
    }
    if (zeile.startsWith("aus") ) { 
      digitalWrite(LED_PIN, LOW);
      ESP_BT.println("LED ausgeschaltet");
    }
  }
  delay(20);
}
