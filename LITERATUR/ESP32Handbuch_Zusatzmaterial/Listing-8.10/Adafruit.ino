#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP32.h>       // Header Cayenne
#include <Wire.h>
#include <Adafruit_Sensor.h>        
#include <Adafruit_BMP280.h>        

#define LED_PIN    2                // GPIO2, Pin G2

const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
char username[] = "8ef6b600-2f90-11e9-809d-0f8fe4cXXXX";
char password[] = "5f72ee148533c416a3aa194c311131507eeXXXX";
char clientID[] = "03121e60-52ec-11ea-b301-fd142d6XXXX";

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
int ledStatus = 0;

CAYENNE_OUT_DEFAULT() { 
  Cayenne.celsiusWrite(0, bmp.readTemperature());
  Cayenne.virtualWrite(1, bmp.readPressure() / 100);
  Cayenne.digitalSensorWrite(2,ledStatus);
}

CAYENNE_IN_DEFAULT() {
  if (request.channel == 3) {
    ledStatus = getValue.asInt();
    digitalWrite(LED_PIN,ledStatus);
  }
}
#include <WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <Adafruit_Sensor.h>        
#include <Adafruit_BMP280.h>

#define LED_PIN    2                // GPIO2, Pin G2            
#define AIO_USERNAME    "adafruitname"
#define AIO_KEY         "aio_YKbw43wiOPG3A6bVGHHjlp53XXXX"

WiFiClient client;                  // WiFi-Objekt
const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
const char* aioServer = "io.adafruit.com";
const int  aioServerPort = 1883;

Adafruit_MQTT_Client mqtt(&client, aioServer, aioServerPort, AIO_USERNAME, AIO_KEY);        // Setup MQTT 
Adafruit_MQTT_Subscribe subLED      = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/ESP32-LED");
Adafruit_MQTT_Publish pubLuftdruck  = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/ESP32-LUFTDRUCK");
Adafruit_MQTT_Publish pubTemperatur = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/ESP32-TEMPERATUR");

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
long millisAlt = -999999;

// MQTT-Verbindung zu io-adafruit herstllen
void MQTT_connect() {
  int8_t ret;
  if (mqtt.connected()) {           // return bei bestehender Verbindung
    return;
  }

  Serial.print("MQTT-Verbindungsaufbau... ");
  int versuche = 3;
  while ((ret = mqtt.connect()) != 0) { // connect gibt bei Erfolg 0 zurück
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("... Fehler, erneuter Versuch in 5 Sekunden...");
    mqtt.disconnect();
    delay(5000);                    // 5 Sekunden warten
    versuche--;
    if (versuche == 0) {
      while (1);
    }
  }
  Serial.println("MQTT verbunden!");
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN,OUTPUT);

  Serial.print("Verbindungsaufbau zu ");   
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mqtt.subscribe(&subLED);               // Feed abonnieren

  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  }  

}

void loop() {
  MQTT_connect();                        // Verbindung sicherstellen
  Adafruit_MQTT_Subscribe *subscription; 
  while ((subscription = mqtt.readSubscription(5000))) {
    if (subscription == &subLED) {
      Serial.print  ("empfangene Daten: ");
      Serial.println ((char *) subLED.lastread);
      if (!strcmp((char*) subLED.lastread, "ON")) {
        digitalWrite(LED_PIN, HIGH);
      } else {
        digitalWrite(LED_PIN, LOW);
      }
    }
  }

  if (millis() - millisAlt > 20000) {   // alle 20 Sekunden
    millisAlt = millis();
    float temperatur = bmp.readTemperature();
    float luftdruck = bmp.readPressure() / 100;    
    Serial.print   ("senden von Temperatur: " + String(temperatur) );
    Serial.println ("  und Luftdruck: " + String(luftdruck) );    
    if (!pubTemperatur.publish(temperatur)) {
      Serial.println("Senden Temperatur fehlgeschlagen");
   } else {
      Serial.println("Senden Temperatur erfolgreich"); 
   }     
   if (!pubLuftdruck.publish(luftdruck)) {
     Serial.println("Senden Luftdruck fehlgeschlagen");
   } else {
     Serial.println("Senden Luftdruck erfolgreich");    
   }  
  }
  
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);  
  digitalWrite(LED_PIN, LOW);  
  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  } 
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);
}

void loop() {
  Cayenne.loop();
}
