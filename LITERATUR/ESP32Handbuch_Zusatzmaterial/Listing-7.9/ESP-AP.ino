#include <WiFi.h>                     // für WiFi 
#include <WiFiClient.h>
#include <WiFiAP.h>

#define LED_PIN            2          // GPIO2

// Definition der AP-Zugangsdaten (Credentials)
const char *ssid     = "ESP32_AP";
const char *password = "1234abcd";
// Objekt für Webserver mit Standard-Port 80
WiFiServer serverWiFi (80); 

// sendet eine Antwort an den WiFi-Client
void wifiSend (WiFiClient client) { 
  // http-Header
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html");
  client.println(); 
  // die HTTP-Antwort endet mit einer weiteren Leerzeile
  client.println();  
}

// bearbeitet eine Anfrage von dem WiFi-Client
void wifiReceive (WiFiClient client) {
  Serial.println("Neue Anfrage.");          // Meldung im seriellen Monitor
  String currentLine = "";                  // Variable f. eingehende Daten
  while (client.connected()) {              // Loop, solange Client verbunden
    if (client.available()) {               // gibt es Bytes zu lesen   
      char c = client.read();               // 1 Byte in Variable c lesen
      Serial.print(c);                      // Ausgabe des Bytes 
      if (c == '\n') {                      // ist Zeichen newline-Zeichen ?
        // das Ende der HTTP-Anfrage ist eine Leerzeile 
        // und zwei newline-Zeichen hintereinander
        if (currentLine.length() == 0) {    // Ende HTTP-Anforderung Client        
          wifiSend(client);            
          break;                            // while beenden client.connected 
        } else {                            // liegt ein newline vor
         currentLine = "";                  // Variable currentLine löschen
        }
      } else if (c != '\r') {               // alles, nur kein Wagenrücklauf
        currentLine += c;                   // Zeichen currentLine hinzufügen
      }
      // Auswertung der Clientanfrage     
      // war die Clientanfrage "GET /H" oder "GET /L":
      if (currentLine.endsWith("GET /H")) {
        digitalWrite(LED_PIN, HIGH);        // GET /H schaltet die LED an
      }
      if (currentLine.endsWith("GET /L")) {
        digitalWrite(LED_PIN, LOW);         // GET /L schaltet die LED aus     
      }  
    }                                       // end if client.available      
  }                                         // end while client.available 
  
  client.stop();                            // Verbindung schließen
  Serial.println("Client Disconnected.");  
  Serial.println();      
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  pinMode(LED_PIN, OUTPUT);
  WiFi.softAP(ssid, password);              
  IPAddress apIP = WiFi.softAPIP();
  Serial.print("AP IP-Adresse: ");
  Serial.println(apIP);
  serverWiFi.begin();
  Serial.println("Server started");
}

void loop() {
  WiFiClient client = serverWiFi.available();  // horcht auf Clientanfragen
  if (client) {                             // fragt ein Client an?
    wifiReceive (client);                   // Anfrage aufbereiten
  }      
  delay(1);
}
