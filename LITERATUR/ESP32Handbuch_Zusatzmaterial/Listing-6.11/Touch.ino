#define PROZENT_SCHWELLENWERT  80
#define TOUCH_PIN              T0           // GPIO4
#define LED_PIN                2            // GPIO2

uint16_t erstWert;

void setupTouch(void) {
  erstWert = touchRead(TOUCH_PIN);
  Serial.print  ("testlesen Touchpin GPIO ");  
  Serial.print  (TOUCH_PIN);  
  Serial.print  (" freilaufend - gelesener Wert "); 
  Serial.println(erstWert);  
  return;
}

void lesenTouch() {
  uint16_t aktuellWert = 0;
  aktuellWert = touchRead(TOUCH_PIN);
  if (aktuellWert < erstWert * PROZENT_SCHWELLENWERT / 100) {
    Serial.print  ("Touch registriert Touchpin GPIO "); 
    Serial.print  (TOUCH_PIN);  
    Serial.print  (" - Wert bei init ");   
    Serial.print  (erstWert);  
    Serial.print  (", nun gelesen ");    
    Serial.println(aktuellWert);  
    digitalWrite (LED_PIN, HIGH);
    delay (1000);       
  } else {
    digitalWrite (LED_PIN, LOW);
    delay (1000); 
  }  
}

void setup() {
  Serial.begin(115200);
  Serial.println("Start des setup");
  setupTouch();
  pinMode(LED_PIN, OUTPUT);
  digitalWrite (LED_PIN, LOW);  
  Serial.println("Ende des setup");  
}

void loop() {
  lesenTouch();
}
