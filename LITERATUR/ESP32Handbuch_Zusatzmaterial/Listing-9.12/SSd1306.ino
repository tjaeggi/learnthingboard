#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/Barokah12pt7b.h>

#define SCREEN_WIDTH 128              // OLED-Display-Breite in Pixel
#define SCREEN_HEIGHT 64              // OLED-Display-Höhe in Pixel

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

void zeichneOled(void) {
  display.clearDisplay();             // Puffer löschen  
  display.setFont();                  // Setzen Standardfont  

  display.setTextSize(1);             // normale Pixel-Größe 1:1 
  display.setTextColor(WHITE);        // weißer Text
  display.setCursor(0,0);             // Start links oben
  display.println("ESP32 !");
  display.println("");
  display.println("");
  
  display.setFont(&Barokah12pt7b);    // neuer Font
  display.setTextSize(1);            
  display.println("ESP32 !");
  display.display();
  delay(2000);
  display.clearDisplay();             // Display löschen 
  display.display();  
}

void setup() {
  Serial.begin(115200);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println("SSD1306-Beginn fehlgeschlagen");
    for(;;); 
  } 
}

void loop() {
  zeichneOled();   
  delay(1000);
}
