#define BLYNK_PRINT Serial       // Monitor Anmeldevorgang
#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

const char* ssid = "ssidRouter";
const char* password = "passwortRouter";
char auth []= "6z5t……..jggvs";   // der Blynk-App-Auth-Token aus der E-Mail

void setup() {
  Blynk.begin(auth, ssid, password);
}

void loop() {
  Blynk.run();
}
