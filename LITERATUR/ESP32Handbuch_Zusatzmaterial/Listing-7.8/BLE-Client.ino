#include "BLEDevice.h"

#define SERVICEUUID            "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_RX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"

static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;
static boolean ledStatus = false;
static BLERemoteCharacteristic* pRemoteCharacteristicRX;
static BLERemoteCharacteristic* pRemoteCharacteristicTX;
static BLEAdvertisedDevice* myDevice;

static void rxCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify) {
    Serial.print("empfangene BLE-Daten: ");
    for (int i = 0; i < length; i++) {
      Serial.print((char)pData[i]);  
    }
    Serial.println();
}

class MyClientCallback : public BLEClientCallbacks {
  void onConnect(BLEClient* pclient) {
  }

  void onDisconnect(BLEClient* pclient) {
    connected = false;
    Serial.println("Verbindung getrennt");
  }
};

bool connectToServer() {
  Serial.print("- Beginn Verbindungsaufbau zu ");
  Serial.println(myDevice->getAddress().toString().c_str());

  // Client einrichten
  BLEClient*  pClient  = BLEDevice::createClient();
  Serial.println("  = Client eingerichtet");
  pClient->setClientCallbacks(new MyClientCallback());

  // Verbindung zu BLE-Server herstellen
  pClient->connect(myDevice);  
  Serial.println("  = BLE-Server-Verbindung hergestellt");

  // Übereinstimmen der Service-UUID überprüfen 
  BLERemoteService* pRemoteService = pClient->getService(SERVICEUUID);
  if (pRemoteService == nullptr) {
    Serial.print("Service-UUIDs stimmen nicht überein: ");
    Serial.println(SERVICEUUID);
    pClient->disconnect();
    return false;
  }
  Serial.println("  = Service gefunden");

  // Übereinstimmen der UUID für den Datenempfang überprüfen 
  pRemoteCharacteristicRX = 
     pRemoteService->getCharacteristic((BLEUUID) CHARACTERISTIC_UUID_RX);
  if (pRemoteCharacteristicRX == nullptr) {
    Serial.print("RX characteristic UUID nicht gefunden: ");
    Serial.println(CHARACTERISTIC_UUID_RX);
    pClient->disconnect();
    return false;
  }

  // Übereinstimmen der UUID für das Senden von Daten überprüfen 
  pRemoteCharacteristicTX = 
     pRemoteService->getCharacteristic((BLEUUID) CHARACTERISTIC_UUID_TX);
  if (pRemoteCharacteristicTX == nullptr) {
    Serial.print("TX characteristic UUID nicht gefunden: ");
    Serial.println(CHARACTERISTIC_UUID_TX);
    pClient->disconnect();
    return false;
  }
   
  Serial.println("  = alle Characteristics gefunden");
  if(pRemoteCharacteristicRX->canNotify())
    pRemoteCharacteristicRX->registerForNotify(rxCallback);

  connected = true;          // Merker setzen
  return true;
}

// passenden Server aus allen "anbietenden" Servern suchen
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
  // wird für jeden gefundenen Server aufgerufen
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    Serial.print("BLE \"anbietender\" Server gefunden: ");
    Serial.println(advertisedDevice.toString().c_str());

    if (advertisedDevice.haveServiceUUID()) {
      Serial.println("- Server sendet eine UUID ");
      if (advertisedDevice.isAdvertisingService((BLEUUID) SERVICEUUID) ) {        
        Serial.println("- Service-UUID entspricht gesuchter UUID" ) ;
        BLEDevice::getScan()->stop();
        myDevice = new BLEAdvertisedDevice(advertisedDevice);
        // Merker für Verbindungsaufbau durchführen (->loop) setzen
        doConnect = true; 
        doScan = true;
      }
    } else {
     Serial.println("BLE-Server hat keine Service-UUID ");       
    }

  }             // onResult
};              // MyAdvertisedDeviceCallbacks

void setup() {
  Serial.begin(115200);
  Serial.println("Scan nach BLE-Servern vorbereiten und starten");

  // BLE-Scanner 
  BLEDevice::init("");  
  BLEScan* pBLEScan = BLEDevice::getScan();
  // Callback für gefundene BLE-Server registrieren
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);        // Active Scanning setzen
  pBLEScan->start(5, false);            // Scan läuft 5 Sekunden
} 

void loop() {
  // passender Server ist durch Scan gefunden
  // es folgt der Verbindungsaufbau  
  if (doConnect == true) {
    if (connectToServer()) {
      Serial.println("==> Verbindung zum BLE-Server steht!");
      Serial.println();
    } else {
      Serial.println("Verbindungsaufbau zum BLE-Server misslungen");
    }
    doConnect = false;
  }

  // bei bestehender Verbindung Update des Sende-Characteristic
  if (connected) {
    if (ledStatus) {
      pRemoteCharacteristicTX->writeValue("ein", 4);
    } else {
      pRemoteCharacteristicTX->writeValue("aus", 4);
    }
    ledStatus = !ledStatus;
  } else if(doScan){
    BLEDevice::getScan()->start(0);  
  }
  
  delay(10000); 
}
