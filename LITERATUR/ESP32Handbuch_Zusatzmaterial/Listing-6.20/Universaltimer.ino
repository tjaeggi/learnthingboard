#define LED_PIN    2                       // LED an GPIO2

volatile int ledStatus = 0;
int interruptZaehlerGesamt;
 
hw_timer_t *timer = NULL;
volatile SemaphoreHandle_t timerSemaphore;

void IRAM_ATTR onTimerEinmal() {
  digitalWrite(LED_PIN, LOW);
}
 
void IRAM_ATTR onTimerMehrfach() {
  ledStatus = !ledStatus;
  digitalWrite(LED_PIN, ledStatus);
  xSemaphoreGiveFromISR(timerSemaphore, NULL);  
}
 
void setup() { 
  Serial.begin(115200); 
  pinMode(LED_PIN, OUTPUT);   
  digitalWrite(LED_PIN, HIGH);  
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimerEinmal, true);
  timerAlarmWrite(timer, 2000000, false);  // für 2 Sekunden
  timerAlarmEnable(timer);
  delay (4000); 

  timerSemaphore = xSemaphoreCreateBinary();   
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimerMehrfach, true);
  timerAlarmWrite(timer, 5000000, true);  // alle 5 Sekunden
  timerAlarmEnable(timer);
}
 
void loop() { 
  double timerSekunden = timerReadSeconds(timer); 
  Serial.print  ("Timer-läuft seit ");
  Serial.println(String (timerSekunden) + " Sekunden");  
  if (xSemaphoreTake(timerSemaphore, 0) == pdTRUE){  
    interruptZaehlerGesamt++; 
    Serial.print("Timer-Interrupt: Gesamtzahl Interrupts: ");
    Serial.println(interruptZaehlerGesamt); 
  }
  delay(1000);
}
