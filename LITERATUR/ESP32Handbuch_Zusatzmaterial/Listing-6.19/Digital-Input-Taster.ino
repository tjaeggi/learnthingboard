#define LEDC_KANAL_0       0         // Kanal 0; einer der 16-LEDC-Kanäle
#define LEDC_AUFLOESUNG    8         // 8-Bit-Tastverhältnisauflösung (0-255)
#define LEDC_FREQ          5000      // 5.000 Hz Frequenz
#define BUZZER_PIN         4         // GPIO4

// 13 Noten bestehend aus Ton, Oktave, Dauer
typedef struct {note_t ton; uint8_t oktave; int laenge;} note; 
note melodie [13] = { 
  {NOTE_F, 3, 500}, {NOTE_D, 3, 500}, {NOTE_D, 3, 1000},
  {NOTE_E, 3, 500}, {NOTE_C, 3, 500}, {NOTE_C, 3, 1000},
  {NOTE_B, 2, 500}, {NOTE_C, 3, 500}, {NOTE_D, 3, 500}, {NOTE_E, 3, 500},
  {NOTE_F, 3, 500}, {NOTE_F, 3, 500}, {NOTE_F, 3, 1000} };     
#define INTERR_PIN    4                  // externer Interrupt an GPIO4
#define debounceZeit  100                // in Millisekunden

volatile int interruptStatus, alteZeit = 0;
volatile int interruptZaehlerGesamt, interruptZaehlerFilter;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// ISR-Routine 
void IRAM_ATTR isrHandler() {
  interruptZaehlerGesamt++; 
  if  ( (millis() - alteZeit) > debounceZeit) {   // Filter
    interruptZaehlerFilter++; 
    portENTER_CRITICAL_ISR(&timerMux);
    interruptStatus++;
    portEXIT_CRITICAL_ISR(&timerMux);  
    alteZeit = millis();                 // letzte Zeit merken
  }   
}
 
void setup() { 
  Serial.begin(115200); 
  pinMode(INTERR_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERR_PIN), isrHandler, FALLING);
}
 
void loop() { 
  if (interruptStatus > 0) { 
    portENTER_CRITICAL(&timerMux);
    interruptStatus--;
    portEXIT_CRITICAL(&timerMux); 
    Serial.println("Impuls-Zähler-Interrupt: ");
    Serial.print  ("  Gesamtzahl Interrupts: ");
    Serial.println(interruptZaehlerGesamt); 
    Serial.print  ("  gefilterte Anzahl    : ");
    Serial.println(interruptZaehlerFilter);
    Serial.println();
  }
}

void buzzer() {
  for (int i = 0; i < 13; i++) {
    ledcWriteNote(LEDC_KANAL_0, melodie[i].ton, melodie[i].oktave); 
    delay (melodie[i].laenge);  
    ledcWrite(LEDC_KANAL_0, 0);           
    delay (30);  
  } 
  ledcWrite(LEDC_KANAL_0, 0);           // Tastverhältnis 0 % 
  delay (1000);                         // Pause zwischen zwei Noten
}

void setup() {
  Serial.begin(115200);  
  ledcSetup(LEDC_KANAL_0, LEDC_FREQ, LEDC_AUFLOESUNG);
  ledcAttachPin(BUZZER_PIN, LEDC_KANAL_0);
}

void loop() {
  buzzer();  
}
