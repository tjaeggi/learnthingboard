#include <Wire.h>
#include <Adafruit_Sensor.h>        // notwenig für exacte Druckberechnung
#include <Adafruit_BMP280.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "thermometer.h"
#include "barometer.h"

#define SDA2_PIN     17                       // SDA-Pin I2C-Kanal 2
#define SCL2_PIN     16     
#define SCREEN_WIDTH 128     // OLED display Breite in Pixel
#define SCREEN_HEIGHT 64     // OLED display Höhe in pixel

TwoWire i2cBus2 = TwoWire (1);               // Objekt Bus 2
Adafruit_BMP280 bmp (&i2cBus2);              // I2C-BMP280 Objekt
float temperatur, luftdruck;

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

void lesenBMP280 () {
  temperatur = bmp.readTemperature();
  //Serial.println("Temperatur: " + String(temperatur) + " *C");
  luftdruck = bmp.readPressure();
  //Serial.println("Luftdruck:  " + String(luftdruck/100) + " hPa");
  //hoehe = bmp.readAltitude(1013.25);
  //Serial.println("Höhe (üM):  " + String(hoehe) + " m");
  //Serial.println();
}

void zeichneOled(void) {
  char str[15];
  float f;
  f=temperatur;   
  sprintf(str, "%d,%01d", (int)f, (int)(f*10)%10);    
  display.clearDisplay();             // Puffer löschen  
  display.setFont();                  // setzen Standardfons  
  display.drawBitmap(
    0, 6, thermometer_bmp, THERMOMETER_WIDTH, THERMOMETER_HEIGHT, WHITE);
  display.setTextSize(3);             // Pixel-Größe 1:3   
  display.setTextColor(WHITE);        // weißer Text
  display.setCursor(40,20);           
  display.println(str);
  display.drawCircle(118, 20, 5, WHITE); 
  display.display(); 
  delay(8000);  

  f=luftdruck/100;   
  if (f > 999) {
    sprintf(str, "%d.%03d,%01d", 
           (int)f/1000, (int)f%1000, (int)(f*10)%10); 
  } else {
    sprintf(str, "%03d,%01d", (int)f%1000, (int)(f*10)%10); 
  }
    
  display.clearDisplay();             // Puffer löschen  
  display.drawBitmap(
    0, 15, barometer_bmp, BAROMETER_WIDTH, BAROMETER_HEIGHT, 1);
  display.setTextSize(2);             // Pixel-Größe 1:2   
  display.setTextColor(WHITE);        // weißer Text
  display.setCursor(40,20);           
  display.println(str);
  display.setCursor(85,40);     
  display.println("hPa");
  display.display(); 
  delay(8000);  
}

void setup() {
  Serial.begin(115200);
  i2cBus2.begin(SDA2_PIN,SCL2_PIN,400000);   
  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden !");
    while (1);
  }  
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println("SSD1306-Beginn fehlgeschlagen");
    for(;;); 
  } 
  // default-Einstellungen lt. Datenblatt
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     // Betzriebsmodus
                  Adafruit_BMP280::SAMPLING_X2,     // Temp. Oversampling 
                  Adafruit_BMP280::SAMPLING_X16,    // Pressure Oversampling 
                  Adafruit_BMP280::FILTER_X16,      // Filter
                  Adafruit_BMP280::STANDBY_MS_500); // Standby-Zeit
}

void loop() {
  lesenBMP280();  
  zeichneOled();   
  delay(4000);
}
