#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "thermometer.h"

#define SCREEN_WIDTH 128     // OLED-Display-Breite in Pixel
#define SCREEN_HEIGHT 64     // OLED-Display-Höhe in Pixel

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

void zeichneOled(void#include <Wire.h>
#include <Adafruit_Sensor.h>        // notwendig für exakte Druckberechnung
#include <Adafruit_BMP280.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "thermometer.h"
#include "barometer.h"

#define SCREEN_WIDTH 128     // OLED-Display-Breite in Pixel
#define SCREEN_HEIGHT 64     // OLED-Display-Höhe in Pixel

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
float temperatur, luftdruck;

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

void lesenBMP280() {
  temperatur = bmp.readTemperature();
  luftdruck = bmp.readPressure();
}

void zeichneOled(void) {
  char str[15];
  float f;
  f=temperatur;   
  sprintf(str, "%d,%01d", (int)f, (int)(f*10)%10);    
  display.clearDisplay();             // Puffer löschen  
  display.setFont();                  // Setzen Standardfont  
  display.drawBitmap(
    0, 6, thermometer_bmp, THERMOMETER_WIDTH, THERMOMETER_HEIGHT, WHITE);
  display.setTextSize(3);             // Pixel-Größe 1:3   
  display.setTextColor(WHITE);        // weißer Text
  display.setCursor(40,20);           
  display.println(str);
  display.drawCircle(118, 20, 5, WHITE); 
  display.display(); 
  delay(8000);  

  f=luftdruck/100;   
  if (f > 999) {
    sprintf(str, "%d.%03d,%01d", 
           (int)f/1000, (int)f%1000, (int)(f*10)%10); 
  } else {
    sprintf(str, "%03d,%01d", (int)f%1000, (int)(f*10)%10); 
  }
  
  display.clearDisplay();             // Puffer löschen  
  display.drawBitmap(
    0, 15, barometer_bmp, BAROMETER_WIDTH, BAROMETER_HEIGHT, 1);
  display.setTextSize(2);             // Pixel-Größe 1:2   
  display.setTextColor(WHITE);        // weißer Text
  display.setCursor(40,20);           
  display.println(str);
  display.setCursor(85,40);     
  display.println("hPa");
  display.display(); 
  delay(8000);  
}

void setup() {
  Serial.begin(115200);
  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  }  
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println("SSD1306-Beginn fehlgeschlagen");
    for(;;); 
  } 
}

void loop() {
  lesenBMP280();  
  zeichneOled();   
}
) {
  display.clearDisplay();             // Puffer löschen    
  display.drawBitmap(
    0, 6, thermometer_bmp, THERMOMETER_WIDTH, THERMOMETER_HEIGHT, WHITE);
  display.display(); 
  delay(4000);
}

void setup() {
  Serial.begin(115200);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println("SSD1306-Beginn fehlgeschlagen");
    for(;;); 
  } 
}

void loop() {
  zeichneOled();   
  delay(4000);
}
