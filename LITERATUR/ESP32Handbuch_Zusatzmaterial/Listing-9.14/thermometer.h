#define THERMOMETER_WIDTH 27
#define THERMOMETER_HEIGHT 50
static const unsigned char PROGMEM thermometer_bmp[] = {
0x00, 0x1f, 0x00, 0x00, 
0x00, 0x7f, 0xc0, 0x00, 
0x01, 0xff, 0xf0, 0x00, 
0x03, 0xff, 0xf8, 0x00, 
0x03, 0xf1, 0xf8, 0x00, 
0x07, 0xc0, 0x7c, 0x00, 
0x0f, 0x80, 0x3e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x00, 0x1e, 0x00, 
0x0f, 0x0e, 0x1e, 0x00, 
0x0f, 0x1f, 0x1e, 0x00, 
0x0f, 0x3f, 0x9e, 0x00, 
0x0f, 0x3f, 0x9e, 0x00, 
0x0f, 0x3f, 0x9e, 0x00, 
0x0f, 0x3f, 0x9e, 0x00, 
0x0f, 0x3f, 0x9e, 0x00, 
0x1f, 0x3f, 0x9f, 0x00, 
0x1f, 0x3f, 0x9f, 0x00, 
0x3e, 0x3f, 0x8f, 0x80, 
0x3e, 0x3f, 0x8f, 0x80, 
0x7c, 0x3f, 0x87, 0xc0, 
0x7c, 0x7f, 0xc7, 0xc0, 
0xf8, 0x7f, 0xc3, 0xe0, 
0xf8, 0xff, 0xe3, 0xe0, 
0xf0, 0xff, 0xe1, 0xe0, 
0xf8, 0xff, 0xe3, 0xe0, 
0x78, 0x7f, 0xc3, 0xc0, 
0x7c, 0x7f, 0xc7, 0xc0, 
0x3c, 0x3f, 0x87, 0x80, 
0x3e, 0x0e, 0x0f, 0x80, 
0x1f, 0x00, 0x1f, 0x00, 
0x1f, 0xc0, 0x7f, 0x00, 
0x0f, 0xf1, 0xfe, 0x00, 
0x07, 0xff, 0xfc, 0x00, 
0x01, 0xff, 0xf0, 0x00, 
0x00, 0x7f, 0xc0, 0x00, 
0x00, 0x0e, 0x00, 0x00
};
