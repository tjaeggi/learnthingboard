#include <EEPROM.h>                         // Bibliothek Flash Memory 
#include <WiFi.h>                           // für WLAN 
#include <WiFiUdp.h>
#include <NTPClient.h>                      // für NTP

#define EEPROM_SIZE 15                      // Speichergröße festlegen

#define NTP_OFFSET 2 * 60 * 60              // in Sekunden
#define NTP_INTERVAL 60 * 1000              // in Millisekunden
#define NTP_ADDRESS "3.de.pool.ntp.org"     // "ptbtime1.ptb.de" 

const char* ssid     = "dieRouterSSID";
const char* password = "dasRouterPW";
IPAddress lclIP (192,168,2,207);
IPAddress gateway (192,168,2,1);            // IP-Adresse des Routers 
IPAddress subnet (255,255,255,0);
IPAddress primaryDNS (8, 8, 8, 8);          // optional
IPAddress secondaryDNS (8, 8, 4, 4);        / /optional

WiFiServer serverWiFi(80);                  // bietet Standard-Port 80     
WiFiClient wifiClient;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

int eepromRGB [5][3];                       // Werte für [Zeitspalten]/[rgb] 

// pins für die Spalten
int pinGND1 = 18;                           // GPIO21, Stundenzehner
int pinGND2 = 19;                           // GPIO1, Stundeneiner
int pinGND3 = 21;                           // GPIO1, Minutenzehner
int pinGND4 = 22;                           // GPIO22, Minuteneiner
int pinGND5 = 23;                           // GPIO23, Block Sekunden
int pinGNDar [5] = {pinGND1, pinGND2, pinGND3, pinGND4, pinGND5};
// Pins für die Zeilen
// 1. Dimension = 1. (unterste) Reihe;  2. Dimension: 
// GPIO-Pin rot, grün, blau
int RGBLedAr [4][3] = {{15,2,0}, {13,12,14}, {27,26,25}, {33,32,5}};

int stunden;
int minuten;
int sekunden;

// Lesen des RGB-Farbstatus
void lesenEeprom() {
  // gesicherten Status aus dem Flash Memory auslesen
  int sp =0, rgb = 0, stat = 0;
  for (int i = 0; i < 15; i++) {    
    stat = EEPROM.read(i);
    if (stat == 255) {
      eepromRGB [sp][rgb]= 1;
    } else {
      eepromRGB [sp][rgb]= 0;
    }
    rgb++;
    if (rgb > 2) {
      sp++;
      rgb = 0;
    }  
  }
}

// Speichern des RGB-Farbstatus
// rx = gerade Zahl   => eeprom = 0   => aus
// rx = ungerade Zahl => eeprom = 255 =>an
void speichernEeprom(int rx) {
  int i = rx / 2;
  int iRest = rx  % 2;
  if (iRest == 0) {
    EEPROM.write(i-1, 0);
  } else {
    EEPROM.write(i, 255);
  }
  EEPROM.commit();       
  return;
}

// Holen der aktuellen Zeit
void holenNTP() {
  timeClient.update();                      // Aktualisieren des Zeitstempels
  stunden = timeClient.getHours();    
  minuten = timeClient.getMinutes();  
  sekunden = timeClient.getSeconds();  
  return;
}

// Anzeige des Spaltenwertes
void displayWert(int w, int spalte){ 
  int r = eepromRGB [spalte][0];
  int g = eepromRGB [spalte][1];
  int b = eepromRGB [spalte][2];
  if (w & 1) {
    if (r) digitalWrite(RGBLedAr[0][0], HIGH);  
    if (g) digitalWrite(RGBLedAr[0][1], HIGH);    
    if (b) digitalWrite(RGBLedAr[0][2], HIGH);   
    delay (2);    
    digitalWrite(RGBLedAr[0][0], LOW);         
    digitalWrite(RGBLedAr[0][1], LOW);        
    digitalWrite(RGBLedAr[0][2], LOW);          
  }
  if (w & 2) {      
    if (r) digitalWrite(RGBLedAr[1][0], HIGH);  
    if (g) digitalWrite(RGBLedAr[1][1], HIGH);      
    if (b) digitalWrite(RGBLedAr[1][2], HIGH);      
    delay (2);    
    digitalWrite(RGBLedAr[1][0], LOW);      
    digitalWrite(RGBLedAr[1][1], LOW);      
    digitalWrite(RGBLedAr[1][2], LOW);             
  }
  if (w & 4) {
    if (r) digitalWrite(RGBLedAr[2][0], HIGH);  
    if (g) digitalWrite(RGBLedAr[2][1], HIGH);      
    if (b) digitalWrite(RGBLedAr[2][2], HIGH);      
    delay (2);    
    digitalWrite(RGBLedAr[2][0], LOW);      
    digitalWrite(RGBLedAr[2][1], LOW);      
    digitalWrite(RGBLedAr[2][2], LOW);          
  }
  if (w & 8) {
    if (r) digitalWrite(RGBLedAr[3][0], HIGH);  
    if (g) digitalWrite(RGBLedAr[3][1], HIGH);      
    if (b) digitalWrite(RGBLedAr[3][2], HIGH);      
    delay (2);    
    digitalWrite(RGBLedAr[3][0], LOW);      
    digitalWrite(RGBLedAr[3][1], LOW);      
    digitalWrite(RGBLedAr[3][2], LOW);       
  }       
  return;
}

// Ausgabe der Zeit
void displayZeit() {
  int s;
  switch (sekunden/15) {
    case 0: s = 1; break;
    case 1: s = 3; break;
    case 2: s = 7; break;
    default  : s = 15; 
  }

  // Stundenzehner  
  digitalWrite(pinGND1 , LOW); 
  displayWert((stunden/10), 0); 
  digitalWrite(pinGND1 , HIGH);  
  // Stundeneiner  
  digitalWrite(pinGND2 , LOW); 
  displayWert((stunden%10), 1); 
  digitalWrite(pinGND2 , HIGH);  
// Minutenzehner  
  digitalWrite(pinGND3 , LOW); 
  displayWert((minuten/10), 2); 
  digitalWrite(pinGND3 , HIGH);  
 // Minuteneiner  
  digitalWrite(pinGND4 , LOW); 
  displayWert((minuten%10), 3); 
  digitalWrite(pinGND4 , HIGH);  
  // Block Sekunden 
  digitalWrite(pinGND5 , LOW); 
  displayWert(s, 4); 
  digitalWrite(pinGND5 , HIGH);  
  return;        
}

// sendet eine Antwort an den WLAN-Client
void wifiSend (WiFiClient client) {  
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html"); 
  client.println();
  // der Inhalt der HTTP-Antwort folgt auf den Header
  client.println(); 
}

// bearbeitet eine Anfrage von dem WLAN-Client
void wifiReceive (WiFiClient client) {
  //Serial.println("Neue Anfrage.");        // Ausgabe einer Meldung im 
                                            // seriellen Monitor
  String currentLine = "";                  // String-Variable f. 
                                            // eingehende Daten
  int cnt = 0;                              // Zähler while client.connected   
  while (client.connected()) {              // while-Shleife, solange Client
    cnt++;                                  // Schleifenzähler um 1 erhöhen    
    if (client.available()) {               // hat Client Bytes?               
      char c = client.read();               // Lesen 1 Byte in Variable c
      // Serial.print(c);                   // Ausgabe des Bytes im SM
      if (c == '\n') {                      // ist Byte ein Newline-Zeichen?
         // das Ende der HTTP-Anfrage ist eine Blank-Zeile 
         // und zwei Newline-Zeichen hintereinander
         if (currentLine.length() == 0) {    // Ende der HTTP-Anforderung
                                             // durch den Client        
          wifiSend(client);            
          break;                            // Beenden der while 
                                            // client.connected-Schleife
        } else {                            // liegt ein Newline vor
          currentLine = "";                 // Variable currentLine löschen
        }
      } else if (c != '\r') {               // alles andere als ein 
                                            // Wagenrücklauf-Zeichen
        currentLine += c;                   // Zeichen currentLine hinzufügen
      }
      // vollständiger Inhalt von currentLine z.B. GET /1 HTTP/1.1
      // war die Client-Anfrage "GET /" gefolgt von einer Zahl?             
      if (currentLine[0] == 'G' &&
         currentLine.length() == 7 &&       
         isDigit (currentLine[5] )) {   
        int n = 0;
        String s1 = "";
        if (isDigit (currentLine[6] )) {    //2-stellig? 
          s1 += currentLine[5]; 
          s1 += currentLine[6];         
        } else {
          s1 += currentLine[5]; 
        }
        n = s1.toInt(); 
        Serial.print("n = "); 
        Serial.println(n);  
        speichernEeprom(n);       
      } 
    }                                       // end if client.available
    
    // erzwingt disconnect bei fehlendem client.available
    if (cnt > 10000) { 
      break;
    }
  }                                         // end while client.connected      
  client.stop();                            // Verbindung schließen
  //Serial.println("Client Disconnected.");
  //Serial.println();      
}

void setup() {
  Serial.begin(115200);
  // EEPROM mit festgelegter Größe initialisieren
  EEPROM.begin(EEPROM_SIZE);
  // gesicherten Status aus dem Flash Memory auslesen
  lesenEeprom();  
  
  // GPIO für die Spalten initialisieren
  for (int i = 0; i < 5; i++) {    
    pinMode(pinGNDar [i], OUTPUT);   
    digitalWrite(pinGNDar [i], HIGH); 
  }  
  
  // GPIO für die Zeilen initialisieren
  for (int i = 0; i < 4; i++) {    
    pinMode(RGBLedAr[i][0], OUTPUT);   
    digitalWrite(RGBLedAr[i][0], LOW); 
    pinMode(RGBLedAr[i][1], OUTPUT);   
    digitalWrite(RGBLedAr[i][1], LOW); 
    pinMode(RGBLedAr[i][2], OUTPUT);   
    digitalWrite(RGBLedAr[i][2], LOW); 
  }   

  if (!WiFi.config(lclIP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA failed to configure ");    
  }
  Serial.print("Verbindungsaufbau zu ");    // Verbindungsaufbau WLAN-Netzwerk
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");  
  Serial.println("WiFi verbunden");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  serverWiFi.begin();  
  
  Serial.println("Start NTP Client!");
  timeClient.begin();
  holenNTP();
  Serial.println("setup beendet"); 
}

void loop() {
  WiFiClient client = serverWiFi.available();  // horcht auf Client-Anfragen
  if (client) {                             // fragt ein Client an?
    wifiReceive (client);                   // Anfrage aufbereiten
  }    
  for (int i = 0; i < 20; i++) { 
    displayZeit();
  }
  holenNTP();
  delay(1);                         // warten
}
