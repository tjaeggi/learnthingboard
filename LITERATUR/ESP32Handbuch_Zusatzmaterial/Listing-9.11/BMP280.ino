// vgl. Arduino-IDE : DATEI • BEISPIELE • ADAFRUIT BMP280 LIBRARY • BMP280TEST).
#include <Wire.h>

#include <Adafruit_Sensor.h>        // notwendig für exakte Druckberechnung
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
float temperatur, luftdruck, hoehe;

void lesenBMP280() {
  temperatur = bmp.readTemperature();
  Serial.println("Temperatur: " + String(temperatur) + " *C");
  luftdruck = bmp.readPressure();
  Serial.println("Luftdruck:  " + String(luftdruck/100) + " hPa");
  hoehe = bmp.readAltitude(1013.25);
  Serial.println("Höhe (üM):  " + String(hoehe) + " m");
  Serial.println();
}

void setup() {
  Serial.begin(115200);
  Serial.println(F("BMP280 test"));

  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  }

  // default-Einstellungen lt. Datenblatt
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     // Betriebsmodus
                  Adafruit_BMP280::SAMPLING_X2,     // Temp. Oversampling 
                  Adafruit_BMP280::SAMPLING_X16,    // Pressure Oversampling 
                  Adafruit_BMP280::FILTER_X16,      // Filter
                  Adafruit_BMP280::STANDBY_MS_500); // Standby-Zeit 
}

void loop() {
  lesenBMP280();
  delay(5000);
}
