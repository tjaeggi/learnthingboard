#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>        
#include <Adafruit_BMP280.h>

const char* ssid = " ssidRouter ";
const char* password = " passwortRouter ";
char auth []= "6z5t……..jggvs";   // der Blynk-App-Auth-Token aus der E-Mail
                          
BlynkTimer timer;  
Adafruit_BMP280 bmp;                // I2C-BMP280-Objekt
float temperatur, luftdruck;                

void sendSensor () {
  temperatur = bmp.readTemperature();
  luftdruck = bmp.readPressure();
  Blynk.virtualWrite(V5, temperatur);
  Blynk.virtualWrite(V6, (luftdruck/100));  
}

void setup() {
  Serial.begin(115200);
  Blynk.begin(auth, ssid, password);

  if (!bmp.begin(0x76)) {
    Serial.println("Kein BMP280-Sensor gefunden!");
    while (1);
  } 
  timer.setInterval(1000L, sendSensor);            // Timer-Intervall in mS 
}

void loop() {
  Blynk.run();
  timer.run();                                     // Timer aufrufen
  if (temperatur > 23) {            // bei Überschreiten der max. Temperatur
    Blynk.email("ihreAdresse", "Temperatur überschritten", "Test Blynk Projekt Email");
    // weitere Aufgaben
    delay (10000);                  // Warten, bis heruntergekühlt
  }
}

