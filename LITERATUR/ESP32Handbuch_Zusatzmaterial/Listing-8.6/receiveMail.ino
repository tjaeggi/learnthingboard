// vgl. Arduino-IDE : DATEI • BEISPIELE • ESP32 MAIL CLIENT - RECEIVE_EMAIL
#include <Arduino.h>
#include "ESP32_MailClient.h"
#include "SD.h"

#define WIFI_SSID         " ssidRouter"
#define WIFI_PASSWORD     " passwortRouter"

IMAPData imapData;                            // Instanz "empfangen" 
unsigned long lastTime = 0;

// Callback Download-Status
void readCallback(ReadStatus msg) {
  Serial.println("INFO: " + msg.info());      // Empfangsstatus ausgeben

  if (msg.status() != "")
    Serial.println("STATUS: " + msg.status());

  if (msg.success()) {                       // empfangene Daten ausgeben
    for (int i = 0; i < imapData.availableMessages(); i++)     {
      Serial.println("=================");
      Serial.println("E-Mail-Nummer: " + imapData.getNumber(i));
      Serial.println("E-Mail-UID   : " + imapData.getUID(i));
      Serial.println("E-Mail-ID    : " + imapData.getMessageID(i));
      Serial.println("Absender    : " + imapData.getFrom(i));
      Serial.println("Empfänger   : " + imapData.getTo(i));
      Serial.println("Datum       : " + imapData.getDate(i));
      Serial.println("Betreff     : " + imapData.getSubject(i));
      Serial.println();
    }
  }
}

void readEmail() {
  Serial.println();
  Serial.println("E-Mails lesen...");

  imapData.setFetchUID("10");
  imapData.setSearchCriteria("");
  MailClient.readMail(imapData);

  imapData.setFetchUID("11");
  imapData.setSearchCriteria("");
  MailClient.readMail(imapData);

  imapData.setFetchUID("12");
  imapData.setSearchCriteria("");
  MailClient.readMail(imapData);
}

void setup() {
  Serial.begin(115200);
  Serial.print("Connecting to AP");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(200);
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // setzen von E-Mail-Host, -Port, -Account und -Passwort
  smtpData.setLogin("smtp.gmail.com", 587, "br@gmail.com", "passw") 

  imapData.setFolder("INBOX");
  imapData.setFetchUID("");
  imapData.setSearchCriteria("UID SEARCH ALL");

  // Download Anlagen  
  imapData.setDownloadAttachment(false);

  // html-E-Mail einbeziehen
  imapData.setHTMLMessage(true);

  // Text-E-Mails einbeziehen
  imapData.setTextMessage(true);

  // maximale Anzahl E-Mails für den Download
  imapData.setSearchLimit(10);

  // Setzen der Text/html-E-Mail-Download-Größe in Bytes
  imapData.setMessageBufferSize(200);

  // Callback registrieren
  imapData.setReadCallback(readCallback);

  MailClient.readMail(imapData);
}

void loop() {}
