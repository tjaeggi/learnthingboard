#define BUTTON_PIN_BITMASK 0x8000        // 1 00000 00000 00000 = 2^15 in hex
#define TIME_TO_SLEEP      5*1000000     // "Schlafzeit" in Mikrosek. = 5 Sek

RTC_DATA_ATTR int bootCount = 0;         // Zähler in den RTC-Speicher legen
int loopCount = 0;                       // Schleifenzähler

void printWeckQuelle(){
  esp_sleep_wakeup_cause_t weckQuelle = esp_sleep_get_wakeup_cause();
  switch(weckQuelle)   {
    case ESP_SLEEP_WAKEUP_EXT0 : 
      Serial.println("Weckquelle ist ein externes RTC_IO-Signal"); 
      break;
    case ESP_SLEEP_WAKEUP_EXT1 : 
      Serial.println("Weckquelle ist ein externes RTC_CNTL-Signal"); 
      break;
    case ESP_SLEEP_WAKEUP_TIMER : 
      Serial.println("Weckquelle ist ein Timer");
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD :
      Serial.println("Weckquelle ist ein Touch-Sensor"); 
      break;
    case ESP_SLEEP_WAKEUP_ULP : 
      Serial.println("Weckquelle ist ein ULP-Programm"); 
      break;
    default : 
      Serial.printf("Weckquelle undefiniert: %d\n", weckQuelle); 
      break;
  }
}

void setup(){
  Serial.begin(115200);
  delay(1000);                              // etwas Zeit 
  ++bootCount;                              // bei jedem Boot + 1 
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33,0); //1 = High, 0 = Low
  esp_sleep_enable_ext1_wakeup(BUTTON_PIN_BITMASK, ESP_EXT1_WAKEUP_ALL_LOW); 
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP); 
}

void loop(){ 
  ++loopCount;
  Serial.print  ("bootCount " + String(bootCount) );
  Serial.println(" / loopCount " + String(loopCount) );                       
  printWeckQuelle();                         // Weckquelle ausgeben
  Serial.println("Start Light-Sleep-Modus");       
  delay (500);                               // Zeit Ausgabe zu komplettieren
  esp_light_sleep_start();                   // in den Light-Sleep-Modus 
  Serial.println("loop Ende");
}
