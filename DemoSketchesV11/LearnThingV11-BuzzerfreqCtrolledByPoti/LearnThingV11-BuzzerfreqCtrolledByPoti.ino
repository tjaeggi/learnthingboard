///////////////////////////////////////////////////////////////////////////////////////////////////
// LearnThingBoard Demo-Sketch
// Buzzer changes Frequency by turning on the Potentiometer-Knob
// Used Pins:
// ==========
// Buzzer-Pin: 21
// Potentiometer-Pin: 4
//
// 24.09.2022 Thomas Jäggi
///////////////////////////////////////////////////////////////////////////////////////////////////

// piezo buzzer on Pin 21
const int TONE_OUTPUT_PIN = 21;

// Potentiometer Output Pin 4
const int POTI_PIN = 4;

// The ESP32 has 16 channels which can generate 16 independent waveforms
// We'll just choose PWM channel 0 here
const int TONE_PWM_CHANNEL = 0; 

const int MIN_FREQ = 10;   // min frequency in hz
const int MAX_FREQ = 4000; // max frequency in hz
const int MAX_ANALOG_VAL = 4095;

void setup() {
  ledcAttachPin(TONE_OUTPUT_PIN, TONE_PWM_CHANNEL);
}

void loop() {
  
  int potiVal = analogRead(POTI_PIN);
  int pwmFreq = map(potiVal, 0, MAX_ANALOG_VAL, MIN_FREQ, MAX_FREQ);
  
  // The ledcWriteTone signature: double ledcWriteTone(uint8_t chan, double freq)
  // See: https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-ledc.c
  ledcWriteTone(TONE_PWM_CHANNEL, pwmFreq);

  delay(50);
}