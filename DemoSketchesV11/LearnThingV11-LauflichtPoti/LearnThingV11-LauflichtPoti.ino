///////////////////////////////////////////////////////////////////////////////////////////////////
// LearnThingBoard Demo-Sketch
// Lauflicht. Geschwindigkeit lässt sich am Potentiometer einstellen.
// Used Pins:
// ==========
// siehe Code.
// 
//
// 24.09.2022 Thomas Jäggi
///////////////////////////////////////////////////////////////////////////////////////////////////

// Pinbelegung der LEDs
int LEDr=12; 
int LEDyw=13;
int LEDgn=14;
int LEDw=15;

// Der Pin des Potentiometer-muss ein ADC-Pin sein. Siehe 
// https://www.olimex.com/Products/IoT/ESP32/ESP32-DevKit-LiPo/resources/ESP32-DevKit-Lipo-GPIOs.png
int potipin = 4;
// potival enthält den Wert des Potis 0..4095. 0=0V, 4095=3.3V
int potival;
// Button-Pins
////////////////////////////////////////////////////////////////////////////////
// Das Array mit den belegten Pins. Die Nullen vorne und hinten sind ein Trick,
// damit sich das Lauflicht, dass sich über mehrere LEDs erstreckt, komplett
// aus dem Bild laufen kann.
int pinArray[] = {0,0,0,LEDr,LEDyw,LEDgn,LEDw,0,0,0};
 
// Die Anzahl der Elemente im Array. Die Elemente sind dann über die
//Indizes [0] bis [9] zu erreichen.
int pinCount = 10;
 
// Der aktive Eintrag des Array, pinArray[2] ist das 3(!) Element. Das wird
// zum Start gesetzt, die beiden hinterherlaufenden Lichter kriegen dann die 
// Einträge [1] und [0], also sind zum Start alle // anzusprechenden Pins = 0
int activePin = 2;
 
// Die Laufrichtung, in der für den nächsten Schritt zum activePin addiert 
// wird. dx ist +1 (aufwärts im Array) oder -1 (abwärts im Array)
int dx = 1;

// Mindestverzögerung zwischen zwei Schritten des Lauflichts. 
// Mit z.B. mintime=20 kann man die maximale Geschwindigkeit begrenzen.
int mintime = 20;
 
// setup() wird einmal bei Programmstart ausgeführt
void setup() {
  // Serielle Übertragung starten
  Serial.begin(115200);
  // Durch das Array gehen, alle Pins auf OUTPUT setzen ausser es kommt 0.
  for (int i=0; i< pinCount; i++) {
    if (pinArray[i] != 0) {
      pinMode(pinArray[i], OUTPUT);
    }
  }
}
 
// loop() wird während des Programmablaufs endlos immer wieder aufgerufen
void loop() {

  // Zuerst schalten wir alle Pins of Array auf LOW, als Hilfsfunktion
  // verwenden wir digitalWriteNot0(), damit die Array-Einträge mit 
  // Wert 0 ignoriert werden.
  for (int i=0; i<pinCount; i++) {
    digitalWriteNot0(pinArray[i], LOW);
  }
 
  // Sind wir am Ende des Arrays angekommen? Wenn ja, dann einen Schritt 
  // zurückgehen (zum vorletzten 0-Element) und mit dx = -1 die Laufrichtung#
  // ändern.
  if (activePin == (pinCount-1)) {
    activePin = (pinCount - 2);
    dx = -1;
  }
 
  // Sind wir am Anfang des Arrays angekommen? Wenn ja, dann einen Schritt 
  // weitergehen (zum 2. 0-Element) und mit dx = 1 die Laufrichtung aufwärts setzen.
  if (activePin == 0) {
    activePin = 1;
    dx = 1;
  }
 
  // Zum aktiven Pin die Laufrichtung hinzuzählen. An den Rändern des Array mit
  // den 3 Nullen sind wir damit mindestens 2 Elemente vom Rand entfernt, haben 
  // also Platz um 2 weitere LEDs leuchten zu lassen
  activePin += dx;
 
  // Potentiometer von Pin 0 einlesen (Wert zwischen 0 und 1023)
  potival = analogRead(potipin);  //liefert Werte von 0..4095
  // Wenn der Wert < 4095 ist, dann Lauflicht. Wenn Wert 4095 (=max=3.3V) dann sind alle LEDs aus.
  if (potival < 4095) {
    // Die LED auf activePin einschalten
    digitalWriteNot0(pinArray[activePin], HIGH);
    // und die LED in entgegengesetzter Laufrichtung davor
    digitalWriteNot0(pinArray[activePin-dx], HIGH);
    // und eine weitere LED in entgegengesetzter Laufrichtung davor
    digitalWriteNot0(pinArray[activePin-2*dx], HIGH);
    // Das funktoniert, weil wir durch die 3 Nullen an den Rändern des Arrays genug
    // Platz haben, um im Array zu lesen, ohnen die Arraygrenzen zu sprengen. Wenn 
    // als Pin eine 0 drinsteht, wird dieses beim Schalten der LEDs einfach ignoriert.
  }
 
  // Debug-Ausgabe für den Seriellen Monitor. Einfach über "Tools" einschalten
  Serial.print(potival);
  Serial.print(" - ");
  Serial.println(activePin);
 
  // Den 1/8-Wert des Potentiometers verwenden wir für die Wartezeit 
  // zwischen den Schritten in Millisekunden. mintime kann als Mindestwert gesetzt 
  // werden, damit wird die maximale Geschwindigkeit des Lauflichts begrenzt.
  delay(mintime+analogRead(potipin)/8);
 
}
 
// digitalWriteNot0() ist eine Hilfsfunktion, die nur dann digitalWrite() ausführt,
// wenn das übergebene Pin nicht 0 ist. Damit werden die Füllelemente des pinArray
// bei der Ausgabe einfach ignoriert.
void digitalWriteNot0(int pin, boolean state) {
  if (pin > 0) {
    digitalWrite(pin, state);
  }
}